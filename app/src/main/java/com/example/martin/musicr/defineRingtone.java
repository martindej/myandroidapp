package com.example.martin.musicr;

import android.content.ContentValues;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;

/**
 * Created by martin on 21/02/2016.
 */
public class defineRingtone implements Runnable {

    Context context;

    public defineRingtone(Context cont) {
        context = cont;

    }

    public void run() {
        String filepath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.mediaMusics";
        String filename = "ringtone.mid";

        File ringtoneFile = new File(filepath,filename);

        ContentValues values = new ContentValues(9);
        values.put(MediaStore.MediaColumns.DATA, ringtoneFile.getAbsolutePath());
        values.put(MediaStore.MediaColumns.TITLE, "ringtone");
        values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/*");
        values.put(MediaStore.Audio.Media.ARTIST, "me");
        values.put(MediaStore.MediaColumns.SIZE, ringtoneFile.length());
        values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, false);
        values.put(MediaStore.Audio.Media.IS_ALARM, false);
        values.put(MediaStore.Audio.Media.IS_MUSIC, false);

        Uri uri = MediaStore.Audio.Media.getContentUriForPath(ringtoneFile.getAbsolutePath());
        context.getContentResolver().delete(
                uri,
                MediaStore.MediaColumns.DATA + "=\""+ ringtoneFile.getAbsolutePath() + "\"", null);

        Uri newUri = context.getContentResolver().insert(uri, values);

        RingtoneManager.setActualDefaultRingtoneUri(
                context, RingtoneManager.TYPE_RINGTONE,
                newUri);

    }

}
