package com.example.martin.musicr;

/**
 * Created by martin on 14/01/2016.
 */
public class Music {

    private String name;
    private Integer distance;
    private String descr;
    private int idImg;



    public Music(String name) {
        this.name = name;
    }

    public Music(String name, String descr) {
        this.name = name;
        this.descr = descr;
    }

    public Music(String name, Integer distance, String descr, int idImg) {
        this.name = name;
        this.distance = distance;
        this.descr = descr;
        this.idImg = idImg;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getDistance() {
        return distance;
    }
    public void setDistance(Integer distance) {
        this.distance = distance;
    }
    public int getIdImg() {
        return idImg;
    }
    public void setIdImg(int idImg) {
        this.idImg = idImg;
    }
}
