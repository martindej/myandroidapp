package com.example.martin.musicr;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by martin on 27/12/2015.
 */
public class RingReceiver extends BroadcastReceiver {

    Thread SOK_1_CLIENT = null;
    Thread defineRingtone = null;
    Handler myHandler = new Handler();
    Handler myHandlerbis = new Handler();
    Handler myHandlerb = new Handler();
  //  FileInputStream fileInputStream;

    private int nbreInstPbis;
    private int nbreInstSbis;
    private int nbreTimebis;
    private int nbreTonalitybis;
    private int nbreTonalitybisbis;
    private int nbreTempobis;

    SharedPreferences sharedpreferences;
    SharedPreferences sharedpreferencesRingtone;
    SharedPreferences sharedpreferencesbis;

    public void createNotification(Context context) {

        // prepare intent which is triggered if the
// notification is selected

        Intent intent = new Intent(context, MainActivity.class);
// use System.currentTimeMillis() to have a unique ID for the pending intent
        PendingIntent pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent, 0);

// build notification
// the addAction re-use the same intent to keep the example short
        Notification n  = new Notification.Builder(context)
                .setContentTitle("New music available")
                .setContentText("Listen me !")
                .setSmallIcon(R.drawable.icon1)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .getNotification();


        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, n);
    }


    @Override
    public void onReceive(Context context, Intent intent) {

        sharedpreferencesRingtone = context.getSharedPreferences("activation", Context.MODE_PRIVATE);

        if (intent.getAction().equals(TelephonyManager.ACTION_PHONE_STATE_CHANGED)) {
            String callState = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            if (callState.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                if(sharedpreferencesRingtone.getBoolean("ringtone",true)) {

                    sharedpreferences = context.getSharedPreferences("generateRingtone", Context.MODE_PRIVATE);
                    sharedpreferencesbis = context.getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);

                    nbreInstPbis = sharedpreferences.getInt("nbreInstP",0);
                    nbreInstSbis =  sharedpreferences.getInt("nbreInstS",0);
                    nbreTimebis =  sharedpreferences.getInt("nbreTime",0);
                    nbreTonalitybis =  sharedpreferences.getInt("nbreTonality",0);
                    nbreTonalitybisbis =  sharedpreferences.getInt("nbreTonality2",0);
                    nbreTempobis =  sharedpreferences.getInt("nbreTempo",0);

                    if (sharedpreferencesbis.getInt("instP",0) == 0) {
                        nbreInstPbis = -1;
                    }
                    if (sharedpreferencesbis.getInt("instS",0) == 0) {
                        nbreInstSbis = -1;
                    }
                    if (sharedpreferencesbis.getInt("time",0) == 0) {
                        nbreTimebis = 0;
                    }
                    if (sharedpreferencesbis.getInt("tonality",0) == 0) {
                        nbreTonalitybis = 0;
                    }
                    if (sharedpreferencesbis.getInt("tempo",0) == 0) {
                        nbreTempobis = 0;
                    }
                    if(sharedpreferencesbis.getInt("tonality2",0) == 0) {
                        nbreTonalitybisbis = 0;
                    }

                    SOK_1_CLIENT = new Thread(new SOK_1_CLIENT(context.getApplicationContext(),nbreInstPbis,nbreInstSbis,nbreTimebis,nbreTonalitybis,nbreTonalitybisbis,nbreTempobis, "ringtonenext"));

                    // SOK_1_CLIENT.setContext(getApplicationContext());
                    SOK_1_CLIENT.start();

                    myHandlerb.postDelayed(new Runnable() {
                        public void run() {
                            File fileTest = new File(Environment.getExternalStorageDirectory() + "/.mediaMusics", "ringtonenext.mid");
                            if(fileTest.exists()) {
                                File from = new File(Environment.getExternalStorageDirectory() + "/.mediaMusics", "ringtone.mid");
                                File to = new File(Environment.getExternalStorageDirectory()  + "/.mediaMusics", "my ringtone.mid");
                                from.renameTo(to);
                            }
                        }
                    }, 5000);//Message will be delivered in 1 second.)

                    myHandlerbis.postDelayed(new Runnable()
                    {
                        public void run()
                        {
                            File fileTest = new File(Environment.getExternalStorageDirectory() + "/.mediaMusics", "ringtonenext.mid");
                            if(fileTest.exists()) {
                                File from = new File(Environment.getExternalStorageDirectory() + "/.mediaMusics", "ringtonenext.mid");
                                File to = new File(Environment.getExternalStorageDirectory() + "/.mediaMusics", "ringtone.mid");
                                from.renameTo(to);
                            }
                        }
                    }, 6000);//Message will be delivered in 1 second.)


                    defineRingtone = new Thread(new defineRingtone(context));
                    myHandler.postDelayed(new Runnable()
                    {
                        public void run()
                        {
                            defineRingtone.start();
                        }
                    }, 10000);//Message will be delivered in 1 second.

                    createNotification(context);

                }
            }
        }
    }
}
