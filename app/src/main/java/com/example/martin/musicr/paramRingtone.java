package com.example.martin.musicr;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.martin.musicr.R;

/**
 * Created by martin on 22/12/2015.
 */

public class ParamRingtone extends Fragment {

    private ToggleButton toggleRandomizer;
    private SeekBar seekBarInstP;
    private SeekBar seekBarInstS;
    private SeekBar seekBarTime;
    private SeekBar seekBarTonality;
    private SeekBar seekBarTonality2;
    private SeekBar seekBarTempo;
    private Button generate;
    private int nbreInstP;
    private int nbreInstS;
    private int nbreTime;
    private int nbreTonality;
    private int nbreTempo;
    private String title;

    private int compt;

    private int nbreInstPbis;
    private int nbreInstSbis;
    private int nbreTimebis;
    private int nbreTonalitybis;
    private int nbreTonalitybisbis;
    private int nbreTempobis;

    private TextView textMajor;
    private TextView textMinor;

    private TextView textInstP;
    private TextView textInstS;
    private TextView textTonality;
    private TextView textTonality2;
    private TextView textTime;
    private TextView textTempo;

    private TextView textInstPstat;
    private TextView textInstSstat;
    private TextView textTonalitystat;
    private TextView textTimestat;
    private TextView textTempostat;


    private ImageButton ImInstP;
    private ImageButton ImInstS;
    private ImageButton ImTime;
    private ImageButton ImTonalitybis;
    private ImageButton ImTonality;
    private ImageButton ImTempo;

    private FrameLayout binstP;
    private FrameLayout binstS;
    private FrameLayout btime;
    private FrameLayout btonality;
    private FrameLayout btonalitybis;
    private FrameLayout btempo;

    private FrameLayout binstPshu;
    private FrameLayout binstSshu;
    private FrameLayout btimeshu;
    private FrameLayout btonalityshu;
    private FrameLayout btonalitybisshu;
    private FrameLayout btemposhu;

    private RelativeLayout rinstP;
    private RelativeLayout rinstS;
    private RelativeLayout rtime;
    private RelativeLayout rtonality;
    private RelativeLayout rtonalitybis;
    private RelativeLayout rtempo;

    private boolean instP = false;
    private boolean instS  = false;
    private boolean time  = false;
    private boolean tonality  = false;
    private boolean tonalitybis  = false;
    private boolean tempo  = false;

    SharedPreferences sharedpreferences;
    SharedPreferences sharedpreferencesbis;

    Thread SOK_1_CLIENT = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.param_ringtone, container, false);
        // toggleRandomizer = (ToggleButton) view.findViewById(R.id.toggleButton);
        seekBarInstS = (SeekBar) view.findViewById(R.id.seekBar3);
        seekBarInstP = (SeekBar) view.findViewById(R.id.seekBar2);
        seekBarTime = (SeekBar) view.findViewById(R.id.seekBarTime);
        seekBarTonality = (SeekBar) view.findViewById(R.id.seekBarTonality);
        seekBarTonality2 = (SeekBar) view.findViewById(R.id.seekBar15);
        seekBarTempo = (SeekBar) view.findViewById(R.id.seekBarTempo);
        generate = (Button) view.findViewById(R.id.button);

        ImInstP = (ImageButton) view.findViewById(R.id.imageButton10);
        ImInstS = (ImageButton) view.findViewById(R.id.imageButton9);
        ImTime = (ImageButton) view.findViewById(R.id.imageButton8);
        ImTonality = (ImageButton) view.findViewById(R.id.imageButton6);
        ImTonalitybis = (ImageButton) view.findViewById(R.id.imageButton3);
        ImTempo = (ImageButton) view.findViewById(R.id.imageButton5);

        binstP = (FrameLayout) view.findViewById(R.id.binstP);
        binstS = (FrameLayout) view.findViewById(R.id.binstS);
        btime = (FrameLayout) view.findViewById(R.id.btime);
        btonality = (FrameLayout) view.findViewById(R.id.btonality);
        btonalitybis = (FrameLayout) view.findViewById(R.id.btonalitybis);
        btempo = (FrameLayout) view.findViewById(R.id.btempo);

      /*  binstPshu = (FrameLayout) view.findViewById(R.id.frameinstp);
        binstSshu = (FrameLayout) view.findViewById(R.id.frameinsts);
        btimeshu = (FrameLayout) view.findViewById(R.id.frametime);
        btonalityshu = (FrameLayout) view.findViewById(R.id.frametonality);
        btonalitybisshu = (FrameLayout) view.findViewById(R.id.frametonalitybis);
        btemposhu = (FrameLayout) view.findViewById(R.id.frametempo); */

        rinstP = (RelativeLayout) view.findViewById(R.id.relativeLayout5);
        rinstS = (RelativeLayout) view.findViewById(R.id.relativeLayout4);
        rtime = (RelativeLayout) view.findViewById(R.id.reltime);
        rtonality = (RelativeLayout) view.findViewById(R.id.relativeLayout3);
        rtonalitybis = (RelativeLayout) view.findViewById(R.id.relativeLayout6);
        rtempo = (RelativeLayout) view.findViewById(R.id.relativeLayout2);

        textInstP = (TextView) view.findViewById(R.id.textView7);
        textInstS = (TextView) view.findViewById(R.id.textView13);
        textTime = (TextView) view.findViewById(R.id.textView15);
        textTonality = (TextView) view.findViewById(R.id.textView16);
        textTonality2 = (TextView) view.findViewById(R.id.textView21);
        textTempo = (TextView) view.findViewById(R.id.textView18);

        textInstPstat = (TextView) view.findViewById(R.id.textView2);
        textInstSstat = (TextView) view.findViewById(R.id.textView12);
        textTimestat = (TextView) view.findViewById(R.id.textView14);
        //  textTonalitystat = (TextView) view.findViewById(R.id.textView16);
        textTempostat = (TextView) view.findViewById(R.id.textView17);

     /*   binstP.setVisibility(FrameLayout.GONE);
        binstS.setVisibility(FrameLayout.GONE);
        btime.setVisibility(FrameLayout.GONE);
        btonality.setVisibility(FrameLayout.GONE);
        btonalitybis.setVisibility(FrameLayout.GONE);
        btempo.setVisibility(FrameLayout.GONE);*/

        // initializeToggle();
        // toggleButtons();
        seekBarFunction();
        generateAction();
        return view;
    }

    private void generateAction() {


        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sharedpreferences = getContext().getSharedPreferences("generate", Context.MODE_PRIVATE);

                nbreInstPbis = sharedpreferences.getInt("nbreInstP",0);
                nbreInstSbis =  sharedpreferences.getInt("nbreInstS",0);
                nbreTimebis =  sharedpreferences.getInt("nbreTime",0);
                nbreTonalitybis =  sharedpreferences.getInt("nbreTonality",0);
                nbreTonalitybisbis =  sharedpreferences.getInt("nbreTonalitybis",0);
                nbreTempobis =  sharedpreferences.getInt("nbreTempo",0);

                if (!instP) {
                    nbreInstPbis = -1;
                }
                if (!instS) {
                    nbreInstSbis = -1;
                }
                if (!time) {
                    nbreTimebis = 0;
                }
                if (!tonality) {
                    nbreTonalitybis = 0;
                }
                if (!tempo) {
                    nbreTempobis = 0;
                }
                if(!tonalitybis) {
                    nbreTonalitybisbis = 0;
                }

                sharedpreferences = getContext().getSharedPreferences("nbreMusic", Context.MODE_PRIVATE);
                compt = sharedpreferences.getInt("nbre", 0);

                if (compt == 0) {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbre",2);
                    editor.commit();
                    compt = 1;
                } else {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbre",compt+1);
                    editor.commit();
                }
               // title = "New music " + String.valueOf(compt);
                title = "GeneratedMusic";

                System.out.println(nbreInstPbis);
                System.out.println(nbreInstSbis);
                System.out.println(nbreTimebis);
                System.out.println(nbreTonalitybis);
                System.out.println(nbreTonalitybisbis);
                System.out.println(nbreTempobis);
                SOK_1_CLIENT = new Thread(new SOK_1_CLIENT(getActivity().getApplicationContext(),nbreInstPbis,nbreInstSbis,nbreTimebis,nbreTonalitybis,nbreTonalitybisbis,nbreTempobis, title));
                // SOK_1_CLIENT.setContext(getApplicationContext());
                SOK_1_CLIENT.start();

                Context context = getActivity().getApplicationContext();
                CharSequence text = "Music generated";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }

  /*  private void toggleButtons() {

        sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
        if(sharedpreferences.getInt("binstP",0) == 1)
        {
            binstP.setVisibility(FrameLayout.VISIBLE);
            instP = true;
        }
        else {
            binstP.setVisibility(FrameLayout.GONE);
            instP = false;
        }

        if(sharedpreferences.getInt("binstS",0) == 1)
        {
            binstS.setVisibility(FrameLayout.VISIBLE);
            instS = true;
        }
        else {
            binstS.setVisibility(FrameLayout.GONE);
            instS = false;
        }

        if(sharedpreferences.getInt("btime",0) == 1)
        {
            btime.setVisibility(FrameLayout.VISIBLE);
            time = true;
        }
        else {
            btime.setVisibility(FrameLayout.GONE);
            time = false;
        }

        if(sharedpreferences.getInt("btonality",0) == 1)
        {
            btonality.setVisibility(FrameLayout.VISIBLE);
            tonality = true;
        }
        else {
            btonality.setVisibility(FrameLayout.GONE);
            tonality = false;
        }

        if(sharedpreferences.getInt("btonalitybis",0) == 1)
        {
            btonalitybis.setVisibility(FrameLayout.VISIBLE);
            tonalitybis = true;
        }
        else {
            btonalitybis.setVisibility(FrameLayout.GONE);
            tonalitybis = false;
        }

        if(sharedpreferences.getInt("btempo",0) == 1)
        {
            btempo.setVisibility(FrameLayout.VISIBLE);
            tempo = true;
        }
        else {
            btempo.setVisibility(FrameLayout.GONE);
            tonality = false;
        }

        ImInstP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);

                if(instP)
                {
                   // binstP.setVisibility(FrameLayout.GONE);
                  //  binstP.setBackgroundResource(R.drawable.circle);
                    rinstP.setBackgroundResource(R.drawable.shapeitembis);
                   // seekBarInstP.setVisibility(SeekBar.GONE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("binstP", 0);
                    editor.commit();

                }
                else {
                   // binstP.setVisibility(FrameLayout.VISIBLE);
                  //  binstP.setBackgroundResource(R.drawable.circlebis);
                    rinstP.setBackgroundResource(R.drawable.shapeitem);
                   // seekBarInstP.setVisibility(SeekBar.VISIBLE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("binstP", 1);
                    editor.commit();

                }

                instP = !instP;
            }
        });

        ImInstS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);

                if(instS)
                {
                    binstS.setVisibility(FrameLayout.GONE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("binstS",0);
                    editor.commit();
                }
                else {
                    binstS.setVisibility(FrameLayout.VISIBLE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("binstS",1);
                    editor.commit();
                }

                instS = !instS;
            }
        });

        ImTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);

                if(time)
                {
                    btime.setVisibility(FrameLayout.GONE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("btime", 0);
                    editor.commit();
                }
                else {
                    btime.setVisibility(FrameLayout.VISIBLE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("btime", 1);
                    editor.commit();
                }
                time = !time;
            }
        });

        ImTonality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);

                if(tonality)
                {
                    btonality.setVisibility(FrameLayout.GONE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("btonality", 0);
                    editor.commit();
                }
                else {
                    btonality.setVisibility(FrameLayout.VISIBLE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("btonality", 1);
                    editor.commit();
                }
                tonality = !tonality;
            }
        });

        ImTonalitybis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);

                if(tonalitybis)
                {
                    btonalitybis.setVisibility(FrameLayout.GONE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("btonalitybis", 0);
                    editor.commit();
                }
                else {
                    btonalitybis.setVisibility(FrameLayout.VISIBLE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("btonalitybis", 1);
                    editor.commit();
                }
                tonalitybis = !tonalitybis;
            }
        });

        ImTempo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);

                if(tempo)
                {
                    btempo.setVisibility(FrameLayout.GONE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("btempo", 0);
                    editor.commit();
                }
                else {
                    btempo.setVisibility(FrameLayout.VISIBLE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("btempo", 1);
                    editor.commit();
                }
                tempo = !tempo;
            }
        });
    } */

 /*   private void initializeToggle(){

        final SharedPreferences preferences = this.getActivity().getSharedPreferences("randomizer", Context.MODE_PRIVATE);
        boolean active = preferences.getBoolean("active", false);
        toggleRandomizer.setChecked(active);

        toggleRandomizer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preferences.edit().putBoolean("active", isChecked).commit();
            }
        });
    }*/

    private void seekBarFunction() {

        sharedpreferences = getContext().getSharedPreferences("generate", Context.MODE_PRIVATE);
        sharedpreferencesbis = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);

        if(sharedpreferencesbis.getInt("instP",0) == 0)
        {
            //  rinstP.setBackgroundResource(R.drawable.shapeitembis);
            binstP.setBackgroundResource(R.color.white);
            textInstPstat.setText("random");
            textInstP.setVisibility(TextView.GONE);
            //  binstPshu.setVisibility(FrameLayout.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ImInstP.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew, getContext().getTheme()));
            } else {
                ImInstP.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew));
            }
            seekBarInstP.setProgress(0);
        }
        else
        {
            seekBarInstP.setProgress(sharedpreferences.getInt("nbreInstP", 0));
            textInstP.setText(String.valueOf(sharedpreferences.getInt("nbreInstP", 0)));
            //    rinstP.setBackgroundResource(R.drawable.shapeitem);
            binstP.setBackgroundResource(R.color.colorprimary);
            textInstPstat.setText("/ 4");
            textInstP.setVisibility(TextView.VISIBLE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ImInstP.setImageDrawable(getResources().getDrawable(R.mipmap.guitarw, getContext().getTheme()));
            } else {
                ImInstP.setImageDrawable(getResources().getDrawable(R.mipmap.guitarw));
            }
            //   binstPshu.setVisibility(FrameLayout.GONE);
        }

        if(sharedpreferencesbis.getInt("instS",0) == 0)
        {
            //   rinstS.setBackgroundResource(R.drawable.shapeitembis);
            binstS.setBackgroundResource(R.color.white);
            textInstSstat.setText("random");
            textInstS.setVisibility(TextView.GONE);
            //  binstSshu.setVisibility(FrameLayout.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ImInstS.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew, getContext().getTheme()));
            } else {
                ImInstS.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew));
            }
            seekBarInstS.setProgress(0);
        }
        else
        {
            seekBarInstS.setProgress(sharedpreferences.getInt("nbreInstS", 0));
            textInstS.setText(String.valueOf(sharedpreferences.getInt("nbreInstS", 0)));
            //  rinstS.setBackgroundResource(R.drawable.shapeitem);
            binstS.setBackgroundResource(R.color.colorsecondary);
            textInstSstat.setText("/ 4");
            textInstS.setVisibility(TextView.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ImInstS.setImageDrawable(getResources().getDrawable(R.mipmap.saxw, getContext().getTheme()));
            } else {
                ImInstS.setImageDrawable(getResources().getDrawable(R.mipmap.saxw));
            }
            //  binstSshu.setVisibility(FrameLayout.GONE);
        }

        if(sharedpreferencesbis.getInt("time",0) == 0)
        {
            //      rtime.setBackgroundResource(R.drawable.shapeitembis);
            btime.setBackgroundResource(R.color.white);
            textTimestat.setText("random");
            textTime.setVisibility(TextView.GONE);
            //   btimeshu.setVisibility(FrameLayout.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ImTime.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew, getContext().getTheme()));
            } else {
                ImTime.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew));
            }
            seekBarTime.setProgress(0);
        }
        else
        {
            seekBarTime.setProgress(sharedpreferences.getInt("nbreTime", 0));
            textTime.setText(String.valueOf(sharedpreferences.getInt("nbreTime", 0)));
            //    rtime.setBackgroundResource(R.drawable.shapeitem);
            btime.setBackgroundResource(R.color.colortime);
            textTimestat.setText("/ 270");
            textTime.setVisibility(TextView.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ImTime.setImageDrawable(getResources().getDrawable(R.mipmap.timew, getContext().getTheme()));
            } else {
                ImTime.setImageDrawable(getResources().getDrawable(R.mipmap.timew));
            }
            //  btimeshu.setVisibility(FrameLayout.GONE);
        }

        if(sharedpreferencesbis.getInt("tonality",0) == 0)
        {
            //   rtonalitybis.setBackgroundResource(R.drawable.shapeitembis);
            btonalitybis.setBackgroundResource(R.color.white);
            textTonality.setText("random");
            //    btonalitybisshu.setVisibility(FrameLayout.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ImTonalitybis.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew, getContext().getTheme()));
            } else {
                ImTonalitybis.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew));
            }
            seekBarTonality.setProgress(0);
        }
        else
        {
            seekBarTonality.setProgress(sharedpreferences.getInt("nbreTonality", 0));
            textTonality.setText(String.valueOf(sharedpreferences.getInt("nbreTonality", 0)));
            //   rtonalitybis.setBackgroundResource(R.drawable.shapeitem);
            btonalitybis.setBackgroundResource(R.color.colortonality2);
            //  textTonality.setVisibility(TextView.VISIBLE);
            //    btonalitybisshu.setVisibility(FrameLayout.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ImTonalitybis.setImageDrawable(getResources().getDrawable(R.mipmap.treblew, getContext().getTheme()));
            } else {
                ImTonalitybis.setImageDrawable(getResources().getDrawable(R.mipmap.treblew));
            }
        }

        if(sharedpreferencesbis.getInt("tonalitybis",0) == 0)
        {
            //   rtonalitybis.setBackgroundResource(R.drawable.shapeitembis);
            btonality.setBackgroundResource(R.color.white);
            textTonality2.setText("random");
            //    btonalitybisshu.setVisibility(FrameLayout.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ImTonality.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew, getContext().getTheme()));
            } else {
                ImTonality.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew));
            }
            seekBarTonality2.setProgress(0);
        }
        else
        {
            seekBarTonality2.setProgress(sharedpreferences.getInt("nbreTonalitybis", 0));
            if(sharedpreferences.getInt("nbreTonalitybis", 0) == 1)
            {
                textTonality2.setText("Major");
            }
            if(sharedpreferences.getInt("nbreTonalitybis", 0) == 2)
            {
                textTonality2.setText("Minor");
            }
            //   rtonalitybis.setBackgroundResource(R.drawable.shapeitem);
            btonality.setBackgroundResource(R.color.colortonality1);
            //  textTonality.setVisibility(TextView.VISIBLE);
            //    btonalitybisshu.setVisibility(FrameLayout.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ImTonality.setImageDrawable(getResources().getDrawable(R.mipmap.treblew, getContext().getTheme()));
            } else {
                ImTonality.setImageDrawable(getResources().getDrawable(R.mipmap.treblew));
            }
        }

        if(sharedpreferencesbis.getInt("tempo",0) == 0)
        {
            //    rtempo.setBackgroundResource(R.drawable.shapeitembis);
            btempo.setBackgroundResource(R.color.white);
            textTempostat.setText("random");
            textTempo.setVisibility(TextView.GONE);
            //    btemposhu.setVisibility(FrameLayout.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ImTempo.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew, getContext().getTheme()));
            } else {
                ImTempo.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew));
            }
            seekBarTempo.setProgress(0);
        }
        else
        {
            seekBarTempo.setProgress(sharedpreferences.getInt("nbreTempo", 0));
            textTempo.setText(String.valueOf(sharedpreferences.getInt("nbreTempo", 0)));
            //   rtempo.setBackgroundResource(R.drawable.shapeitem);
            btempo.setBackgroundResource(R.color.colortempo);
            textTempostat.setText("/ 200 bpm");
            textTempo.setVisibility(TextView.VISIBLE);
            //    btemposhu.setVisibility(FrameLayout.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ImTempo.setImageDrawable(getResources().getDrawable(R.mipmap.speedw, getContext().getTheme()));
            } else {
                ImTempo.setImageDrawable(getResources().getDrawable(R.mipmap.speedw));
            }
        }

        seekBarTonality2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    tonalitybis = false;
                    btonality.setBackgroundResource(R.color.white);
                    textTonality2.setText("random");
                    //    btonalitybisshu.setVisibility(FrameLayout.VISIBLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ImTonality.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew, getContext().getTheme()));
                    } else {
                        ImTonality.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew));
                    }
                    sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("tonalitybis", 0);
                    editor.commit();
                } else {
                    tonalitybis = true;
                    //    rtonalitybis.setBackgroundResource(R.drawable.shapeitem);
                    btonality.setBackgroundResource(R.color.colortonality1);
                    nbreTonalitybis = progress;
                    // textTonality.setVisibility(TextView.VISIBLE);
                    //   btonalitybisshu.setVisibility(FrameLayout.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ImTonality.setImageDrawable(getResources().getDrawable(R.mipmap.treblew, getContext().getTheme()));
                    } else {
                        ImTonality.setImageDrawable(getResources().getDrawable(R.mipmap.treblew));
                    }
                    if(progress == 1)
                    {
                        textTonality2.setText("Major");
                    }
                    if(progress == 2)
                    {
                        textTonality2.setText("Minor");
                    }

                    sharedpreferences = getContext().getSharedPreferences("generate", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreTonalitybis", nbreTonalitybis);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("tonalitybis", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarInstP.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub
                if (progress == 0) {
                    instP = false;
                    //   rinstP.setBackgroundResource(R.drawable.shapeitembis);
                    binstP.setBackgroundResource(R.color.white);
                    textInstPstat.setText("random");
                    textInstP.setVisibility(TextView.GONE);
                    //    binstPshu.setVisibility(FrameLayout.VISIBLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ImInstP.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew, getContext().getTheme()));
                    } else {
                        ImInstP.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew));
                    }
                    sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("instP", 0);
                    editor.commit();
                } else {
                    instP = true;
                    //    rinstP.setBackgroundResource(R.drawable.shapeitem);
                    binstP.setBackgroundResource(R.color.colorprimary);
                    nbreInstP = progress;
                    textInstPstat.setText("/ 4");
                    textInstP.setVisibility(TextView.VISIBLE);
                    //   binstPshu.setVisibility(FrameLayout.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ImInstP.setImageDrawable(getResources().getDrawable(R.mipmap.guitarw, getContext().getTheme()));
                    } else {
                        ImInstP.setImageDrawable(getResources().getDrawable(R.mipmap.guitarw));
                    }
                    textInstP.setText(String.valueOf(nbreInstP));
                    sharedpreferences = getContext().getSharedPreferences("generate", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreInstP", nbreInstP);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("instP", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarInstS.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub

                if (progress == 0) {
                    instS = false;
                    //    rinstS.setBackgroundResource(R.drawable.shapeitembis);
                    binstS.setBackgroundResource(R.color.white);
                    textInstSstat.setText("random");
                    textInstS.setVisibility(TextView.GONE);
                    //   binstSshu.setVisibility(FrameLayout.VISIBLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ImInstS.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew, getContext().getTheme()));
                    } else {
                        ImInstS.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew));
                    }
                    sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("instS", 0);
                    editor.commit();
                } else {
                    instS = true;
                    //   rinstS.setBackgroundResource(R.drawable.shapeitem);
                    binstS.setBackgroundResource(R.color.colorsecondary);
                    nbreInstS = progress -1;
                    textInstSstat.setText("/ 4");
                    textInstS.setVisibility(TextView.VISIBLE);
                    //    binstSshu.setVisibility(FrameLayout.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ImInstS.setImageDrawable(getResources().getDrawable(R.mipmap.saxw, getContext().getTheme()));
                    } else {
                        ImInstS.setImageDrawable(getResources().getDrawable(R.mipmap.saxw));
                    }
                    textInstS.setText(String.valueOf(nbreInstS));
                    sharedpreferences = getContext().getSharedPreferences("generate", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreInstS", nbreInstS);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("instS", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    time = false;
                    //  rtime.setBackgroundResource(R.drawable.shapeitembis);
                    btime.setBackgroundResource(R.color.white);
                    textTimestat.setText("random");
                    textTime.setVisibility(TextView.GONE);
                    //    btimeshu.setVisibility(FrameLayout.VISIBLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ImTime.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew, getContext().getTheme()));
                    } else {
                        ImTime.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew));
                    }
                    sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("time", 0);
                    editor.commit();
                } else {
                    time = true;
                    //   rtime.setBackgroundResource(R.drawable.shapeitem);
                    btime.setBackgroundResource(R.color.colortime);
                    nbreTime = progress + 99;
                    textTimestat.setText("/ 270");
                    textTime.setVisibility(TextView.VISIBLE);
                    //   btimeshu.setVisibility(FrameLayout.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ImTime.setImageDrawable(getResources().getDrawable(R.mipmap.timew, getContext().getTheme()));
                    } else {
                        ImTime.setImageDrawable(getResources().getDrawable(R.mipmap.timew));
                    }
                    textTime.setText(String.valueOf(nbreTime));
                    sharedpreferences = getContext().getSharedPreferences("generate", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreTime", nbreTime);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("time", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarTonality.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    tonality = false;
                    //  rtonalitybis.setBackgroundResource(R.drawable.shapeitembis);
                    btonalitybis.setBackgroundResource(R.color.white);
                    textTonality.setText("random");
                    //    btonalitybisshu.setVisibility(FrameLayout.VISIBLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ImTonalitybis.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew, getContext().getTheme()));
                    } else {
                        ImTonalitybis.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew));
                    }
                    sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("tonality", 0);
                    editor.commit();
                } else {
                    tonality = true;
                    //    rtonalitybis.setBackgroundResource(R.drawable.shapeitem);
                    btonalitybis.setBackgroundResource(R.color.colortonality2);
                    nbreTonality = progress;
                    // textTonality.setVisibility(TextView.VISIBLE);
                    //   btonalitybisshu.setVisibility(FrameLayout.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ImTonalitybis.setImageDrawable(getResources().getDrawable(R.mipmap.treblew, getContext().getTheme()));
                    } else {
                        ImTonalitybis.setImageDrawable(getResources().getDrawable(R.mipmap.treblew));
                    }
                    sharedpreferences = getContext().getSharedPreferences("generate", Context.MODE_PRIVATE);
                    int Ton = sharedpreferences.getInt("nbreTonalitybis",0);

                    if(progress == 1){
                        if(Ton == 1)
                        {
                            textTonality.setText("C#");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("A#");
                        }
                        else {
                            textTonality.setText("C# - A#");
                        }
                    }
                    if(progress == 2){
                        if(Ton == 1)
                        {
                            textTonality.setText("F#");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("D#");
                        }
                        else {
                            textTonality.setText("F# - D#");
                        }
                    }
                    if(progress == 3){
                        if(Ton == 1)
                        {
                            textTonality.setText("B");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("G#");
                        }
                        else {
                            textTonality.setText("B - G#");
                        }
                    }
                    if(progress == 4){
                        if(Ton == 1)
                        {
                            textTonality.setText("E");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("C#");
                        }
                        else {
                            textTonality.setText("E - C#");
                        }
                    }
                    if(progress == 5){
                        if(Ton == 1)
                        {
                            textTonality.setText("A");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("F#");
                        }
                        else {
                            textTonality.setText("A - F#");
                        }
                    }
                    if(progress == 6){
                        if(Ton == 1)
                        {
                            textTonality.setText("D");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("B");
                        }
                        else {
                            textTonality.setText("D - B");
                        }
                    }
                    if(progress == 7){
                        if(Ton == 1)
                        {
                            textTonality.setText("G");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("E");
                        }
                        else {
                            textTonality.setText("G - E");
                        }
                    }
                    if(progress == 8){
                        if(Ton == 1)
                        {
                            textTonality.setText("C");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("A");
                        }
                        else {
                            textTonality.setText("C - A");
                        }
                    }
                    if(progress == 9){
                        if(Ton == 1)
                        {
                            textTonality.setText("F");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("D");
                        }
                        else {
                            textTonality.setText("F - D");
                        }
                    }
                    if(progress == 10){
                        if(Ton == 1)
                        {
                            textTonality.setText("Bb");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("G");
                        }
                        else {
                            textTonality.setText("Bb - G");
                        }
                    }
                    if(progress == 11){
                        if(Ton == 1)
                        {
                            textTonality.setText("Eb");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("C");
                        }
                        else {
                            textTonality.setText("Eb - C");
                        }
                    }
                    if(progress == 12){
                        if(Ton == 1)
                        {
                            textTonality.setText("Ab");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("F");
                        }
                        else {
                            textTonality.setText("Ab - F");
                        }
                    }
                    if(progress == 13){
                        if(Ton == 1)
                        {
                            textTonality.setText("Db");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("Bb");
                        }
                        else {
                            textTonality.setText("Db - Bb");
                        }
                    }
                    if(progress == 14){
                        if(Ton == 1)
                        {
                            textTonality.setText("Gb");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("Eb");
                        }
                        else {
                            textTonality.setText("Gb - Eb");
                        }
                    }
                    if(progress == 15){
                        if(Ton == 1)
                        {
                            textTonality.setText("Cb");
                        }
                        if(Ton == 2)
                        {
                            textTonality.setText("Ab");
                        }
                        else {
                            textTonality.setText("Cb - Ab");
                        }
                    }

                    sharedpreferences = getContext().getSharedPreferences("generate", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreTonality", nbreTonality);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("tonality", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarTempo.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    tempo = false;
                    //   rtempo.setBackgroundResource(R.drawable.shapeitembis);
                    btempo.setBackgroundResource(R.color.white);
                    textTempostat.setText("random");
                    textTempo.setVisibility(TextView.GONE);
                    //   btemposhu.setVisibility(FrameLayout.VISIBLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ImTempo.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew, getContext().getTheme()));
                    } else {
                        ImTempo.setImageDrawable(getResources().getDrawable(R.mipmap.shufflew));
                    }
                    sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("tempo", 0);
                    editor.commit();
                } else {
                    tempo = true;
                    //  rtempo.setBackgroundResource(R.drawable.shapeitem);
                    btempo.setBackgroundResource(R.color.colortempo);
                    nbreTempo = progress + 39;
                    textTempostat.setText("/ 200 bpm");
                    textTempo.setVisibility(TextView.VISIBLE);
                    //   btemposhu.setVisibility(FrameLayout.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ImTempo.setImageDrawable(getResources().getDrawable(R.mipmap.speedw, getContext().getTheme()));
                    } else {
                        ImTempo.setImageDrawable(getResources().getDrawable(R.mipmap.speedw));
                    }
                    textTempo.setText(String.valueOf(nbreTempo));
                    sharedpreferences = getContext().getSharedPreferences("generate", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreTempo", nbreTempo);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("tempo", 1);
                    editorbis.commit();
                }
            }
        });
    }
}


