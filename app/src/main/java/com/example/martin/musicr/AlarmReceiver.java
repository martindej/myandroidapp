package com.example.martin.musicr;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.WakefulBroadcastReceiver;

import java.io.File;

/**
 * Created by martin on 26/02/2016.
 */
public class AlarmReceiver extends WakefulBroadcastReceiver {

    Thread SOK_1_CLIENT = null;
    Thread defineSnooze = null;
    Handler myHandler = new Handler();
    Handler myHandlerbis = new Handler();
    Handler myHandlerb = new Handler();
    //  FileInputStream fileInputStream;

    private int nbreInstPbis;
    private int nbreInstSbis;
    private int nbreTimebis;
    private int nbreTonalitybis;
    private int nbreTonalitybisbis;
    private int nbreTempobis;

    SharedPreferences sharedpreferencesSnooze;
    SharedPreferences sharedpreferences;
    SharedPreferences sharedpreferencesbis;

    public void createNotification(Context context) {

        // prepare intent which is triggered if the
// notification is selected

        Intent intent = new Intent(context, MainActivity.class);
// use System.currentTimeMillis() to have a unique ID for the pending intent
        PendingIntent pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent, 0);

// build notification
// the addAction re-use the same intent to keep the example short
        Notification n  = new Notification.Builder(context)
                .setContentTitle("New music available")
                .setContentText("Listen me !")
                .setSmallIcon(R.drawable.icon1)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .getNotification();


        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, n);
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
/*
        Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alarmUri == null) {
            alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }
        Ringtone ringtone = RingtoneManager.getRingtone(context, alarmUri);   */
        sharedpreferencesSnooze = context.getSharedPreferences("activation", Context.MODE_PRIVATE);

        if(sharedpreferencesSnooze.getBoolean("snooze",true)) {

            sharedpreferences = context.getSharedPreferences("generateSnooze", Context.MODE_PRIVATE);
            sharedpreferencesbis = context.getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);

            nbreInstPbis = sharedpreferences.getInt("nbreInstP",0);
            nbreInstSbis =  sharedpreferences.getInt("nbreInstS",0);
            nbreTimebis =  sharedpreferences.getInt("nbreTime",0);
            nbreTonalitybis =  sharedpreferences.getInt("nbreTonality",0);
            nbreTonalitybisbis =  sharedpreferences.getInt("nbreTonality2",0);
            nbreTempobis =  sharedpreferences.getInt("nbreTempo",0);

            if (sharedpreferencesbis.getInt("instP",0) == 0) {
                nbreInstPbis = -1;
            }
            if (sharedpreferencesbis.getInt("instS",0) == 0) {
                nbreInstSbis = -1;
            }
            if (sharedpreferencesbis.getInt("time",0) == 0) {
                nbreTimebis = 0;
            }
            if (sharedpreferencesbis.getInt("tonality",0) == 0) {
                nbreTonalitybis = 0;
            }
            if (sharedpreferencesbis.getInt("tempo",0) == 0) {
                nbreTempobis = 0;
            }
            if(sharedpreferencesbis.getInt("tonality2",0) == 0) {
                nbreTonalitybisbis = 0;
            }

            SOK_1_CLIENT = new Thread(new SOK_1_CLIENT(context.getApplicationContext(),nbreInstPbis,nbreInstSbis,nbreTimebis,nbreTonalitybis,nbreTonalitybisbis,nbreTempobis, "snoozenext"));
            SOK_1_CLIENT.start();

            myHandlerb.postDelayed(new Runnable() {
                public void run() {
                    File fileTest = new File(Environment.getExternalStorageDirectory() + "/.mediaMusics", "snoozenext.mid");
                    if(fileTest.exists()) {
                        File from = new File(Environment.getExternalStorageDirectory() + "/.mediaMusics", "snooze.mid");
                        File to = new File(Environment.getExternalStorageDirectory()  + "/.mediaMusics", "my snooze.mid");
                        from.renameTo(to);
                    }
                }
            }, 5000);//Message will be delivered in 1 second.)

            myHandlerbis.postDelayed(new Runnable()
            {
                public void run()
                {
                    File fileTest = new File(Environment.getExternalStorageDirectory() + "/.mediaMusics", "snoozenext.mid");
                    if(fileTest.exists()) {
                        File from = new File(Environment.getExternalStorageDirectory() + "/.mediaMusics", "snoozenext.mid");
                        File to = new File(Environment.getExternalStorageDirectory() + "/.mediaMusics", "snooze.mid");
                        from.renameTo(to);
                    }
                }
            }, 6000);//Message will be delivered in 1 second.)


            defineSnooze = new Thread(new defineSnooze(context));
            myHandler.postDelayed(new Runnable()
            {
                public void run()
                {
                    defineSnooze.start();
                }
            }, 10000);

            createNotification(context);

        }

    }
}
