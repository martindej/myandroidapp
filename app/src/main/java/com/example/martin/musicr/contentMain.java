package com.example.martin.musicr;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TabHost;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import android.os.Handler;

import com.example.martin.musicr.R;

/**
 * Created by martin on 22/12/2015.
 */

public class contentMain extends Fragment {

    ListView listOfRingtones;
   // private List<String> listMusic = new ArrayList<String>();

    ProgressBar progressBar;
    ImageButton play_button, pause_button;
    MediaPlayer player = new MediaPlayer();
    TextView titleMusic;
   // Handler seekHandler;
    String title;
    View view;
    String[] listMusic;
    InputStream mFile = null;
    FileInputStream fileInputStream;
    Handler seekHandler = new Handler();
    SharedPreferences sharedpreferences;

    // The data to show
    List<Music> musicList = new ArrayList<Music>();
    MusicAdapter aAdpt;

    Handler myHandler = new Handler();

    Thread SOK_1_CLIENT = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.content_main, container, false);
        listOfRingtones = (ListView) view.findViewById(R.id.list_of_ringtones);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar2);
        play_button = (ImageButton) view.findViewById(R.id.imageButton);
       // titleMusic = (TextView) view.findViewById(R.id.titleMusic);
        play_button.setOnClickListener(new View.OnClickListener()
        {
               @Override
               public void onClick(View view) {

                   if(play_button.isSelected())
                   {
                       player.pause();
                       play_button.setSelected(false);
                   }
                   else
                   {
                       player.start();
                       play_button.setSelected(true);
                   }

               }
        });

       // player = MediaPlayer.create(getActivity(), R.raw.essai1);
       // seek_bar.setMax(player.getDuration());

      //  System.out.println("******************************************************************************"+Settings.System.getString(getContext().getContentResolver(), Settings.System.NEXT_ALARM_FORMATTED));

        initialisation();
        getRawFiles();
        setListenerList();


        aAdpt = new MusicAdapter(musicList,getContext());
        listOfRingtones.setAdapter(aAdpt);

        return view;
    }

    private void initialisation() {

        // first generation of ringtone at the first start of the app

        SharedPreferences preferences = getContext().getSharedPreferences("firstuse", Context.MODE_PRIVATE);
        if(!preferences.contains("ringtone")) {

            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("ringtone", true);
            editor.commit();

            String folder_main = ".mediaMusics";

            File f = new File(Environment.getExternalStorageDirectory(), folder_main);
            if (!f.exists()) {
                f.mkdirs();
            }

            myHandler.postDelayed(new Runnable() {
                public void run() {
                    SOK_1_CLIENT = new Thread(new SOK_1_CLIENT(getContext().getApplicationContext(), -1, -1, 0, 0, 0, 0, "ringtone"));
                    // SOK_1_CLIENT.setContext(getApplicationContext());
                    SOK_1_CLIENT.start();
                }
            }, 1000);

            myHandler.postDelayed(new Runnable()
            {
                public void run()
                {
                    SOK_1_CLIENT = new Thread(new SOK_1_CLIENT(getContext().getApplicationContext(), -1, -1, 0, 0, 0, 0, "ringtonenext"));
                    // SOK_1_CLIENT.setContext(getApplicationContext());
                    SOK_1_CLIENT.start();
                }
            }, 2000);

            myHandler.postDelayed(new Runnable()
            {
                public void run()
                {
                    SOK_1_CLIENT = new Thread(new SOK_1_CLIENT(getContext().getApplicationContext(), -1, -1, 0, 0, 0, 0, "snooze"));
                    // SOK_1_CLIENT.setContext(getApplicationContext());
                    SOK_1_CLIENT.start();
                }
            }, 3000);

            myHandler.postDelayed(new Runnable() {
                public void run() {
                    SOK_1_CLIENT = new Thread(new SOK_1_CLIENT(getContext().getApplicationContext(), -1, -1, 0, 0, 0, 0, "snoozenext"));
                    // SOK_1_CLIENT.setContext(getApplicationContext());
                    SOK_1_CLIENT.start();
                }
            }, 4000);

        }

/*

        String filepath = Environment.getExternalStorageDirectory().getAbsolutePath();
        String filename = "ringtone.mid";

      //  getContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + filepath)));

        File ringtoneFile = new File(filepath,filename);

        System.out.println("***************file ringtone :  "+ringtoneFile);
        System.out.println("***************path file ringtone :  "+ringtoneFile.getAbsolutePath());

        ContentValues values = new ContentValues(9);
        values.put(MediaStore.MediaColumns.DATA, ringtoneFile.getAbsolutePath());
        values.put(MediaStore.MediaColumns.TITLE, "ringtone");
        values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/*");
        values.put(MediaStore.Audio.Media.ARTIST, "me");
        values.put(MediaStore.MediaColumns.SIZE, ringtoneFile.length());
        values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, false);
        values.put(MediaStore.Audio.Media.IS_ALARM, false);
        values.put(MediaStore.Audio.Media.IS_MUSIC, false);

        Uri uri = MediaStore.Audio.Media.getContentUriForPath(ringtoneFile.getAbsolutePath());
        getActivity().getContentResolver().delete(
                uri,
                MediaStore.MediaColumns.DATA + "=\""+ ringtoneFile.getAbsolutePath() + "\"", null);

      //  getActivity().getContentResolver().delete(MediaStore.Audio.Media.INTERNAL_CONTENT_URI,"TITLE='essai1'", null);

        Uri newUri = getActivity().getContentResolver().insert(uri, values);

        RingtoneManager.setActualDefaultRingtoneUri(
                getActivity(), RingtoneManager.TYPE_RINGTONE,
                newUri);
*/
   /*     MediaPlayer mp = new MediaPlayer();

        try {
            mp.setDataSource(filepath+File.separator+filename);
            mp.prepare();
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        } */
    }
/*
    public static String getMusicContentUriFromFilePath(Context ctx, String filePath) {

        ContentResolver contentResolver = ctx.getContentResolver();
        String audioUriStr = null;
        long audioId = -1;

        Uri videosUri = MediaStore.Audio.Media.getContentUri("external");

        String[] projection = {MediaStore.Audio.AudioColumns._ID};

        // TODO This will break if we have no matching item in the MediaStore.
        Cursor cursor = contentResolver.query(videosUri, projection, MediaStore.Audio.AudioColumns.DATA + " LIKE ?", new String[] { filePath }, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(projection[0]);
        audioId = cursor.getLong(columnIndex);

      //  Log.d(Prefs.TAG, "Video ID is " + videoId);
        cursor.close();
        if (audioId != -1 ) audioUriStr = videosUri.toString() + "/" + audioId;
        return audioUriStr;
    }
*/
    public void setListenerList() {
        listOfRingtones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (player.isPlaying())
                {
                    player.stop();
                    player.release();
                }

                play_button.setSelected(true);

               // title = listMusic.get(position);

               // player = MediaPlayer.create(getActivity(), R.raw.m_69);

               // player.setDataSource(R.raw);

               // MediaPlayer player = new MediaPlayer();
               // String path = "android.resource://"+getActivity().getPackageName()+"/raw/"+title;

               // mFile = getContext().getDir("media", Context.MODE_PRIVATE).getAbsolutePath();
               // mFile = Context.openFileOutput("media", Context.MODE_PRIVATE).getAbsolutePath();

                title = musicList.get(position).getName() + ".mid";

                String filePath = Environment.getExternalStorageDirectory().getPath() + "/.mediaMusics"+ "/" + title;
                File file = new File(filePath);
              //  FileInputStream inputStream = new FileInputStream(file);

                try {
                  //  fileInputStream = getContext().openFileInput(title); //, Context.MODE_PRIVATE
                    fileInputStream = new FileInputStream(file);
                }
                catch (FileNotFoundException e) {
                    e.printStackTrace(); // Handle the error here
                }

             //   FileInputStream fileInputStream = new FileInputStream(mFile); //"/data/data/com.example.martin.musicr/newMusic.mid"

                //String ave_file_name = "my_media_content.ogg";

              //  fileM = Context.openFileOutput("newMusic.mid", Context.MODE_PRIVATE);

               // titleMusic.setText("lecture");

                try {
                    player = new MediaPlayer();
                   // player.reset();
                    //player.setDataSource(path);
                   // player.setOnCompletionListener(this.getActivity());
                   // player.setOnErrorListener(this);
                    player.setDataSource(fileInputStream.getFD());                      //getActivity(), Uri.parse(path));
                    // player.prepare();
                    //player.create(getActivity(), R.raw.essai1);
                    // player.prepare();
                    player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer player) {
                            player.start();
                            progressBar.setMax(player.getDuration());
                            seekUpdation();
                        }
                    });
                    player.prepareAsync();


                    // player.create(getActivity(), Uri.parse(path));
                   // player.start();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

              /*  public void onPrepared(MediaPlayer player) {
                    player.start();
                    seek_bar.setMax(player.getDuration());
                }*/
            }
        });
    }

    public void getRawFiles(){
       // Field[] fields = R.raw.class.getFields();

       // Field[] fields =  fileList();

       // FileInputStream openFileInput (String name)
        //File getFilesDir();
        //listMusic =  fileList();
        int isEquals = 0;

       // listMusic = getContext().fileList();
        String path = Environment.getExternalStorageDirectory() + "/.mediaMusics";
        listMusic = new File(path).list();

       // ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listMusic);

        for(int i=0;i<listMusic.length; i++) {

            isEquals = 0;

            if(listMusic[i].contains(".mid") && !listMusic[i].equals("ringtone.mid") && !listMusic[i].equals("ringtonenext.mid") && !listMusic[i].equals("snoozenext.mid") && !listMusic[i].equals("snooze.mid")) {
                for (int j = 0; j < musicList.size(); j++) {
                    if (musicList.get(j).getName().equals(listMusic[i].substring(0,listMusic[i].length()-4))) {
                        isEquals = 1;
                    }
                }
                if (isEquals == 0) {
                    musicList.add(new Music(listMusic[i].substring(0,listMusic[i].length()-4)));
                }
            }
        }
        // loop for every file in raw folder
     /*   for(int i=0; i < fields.length; i++){
            ringtones.add(fields[i].getName());
        }*/

    /*    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, ringtones) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView item = (TextView)super.getView(position, convertView, parent);
                item.setText(ringtones.get(position));
                return item;
            }
        };*/

       // listOfRingtones.setAdapter(adapter);
    }

    Runnable run = new Runnable() {
        @Override
        public void run() {
            seekUpdation(); }
    };

    public void seekUpdation() {
        progressBar.setProgress(
                player.getCurrentPosition());
                seekHandler.postDelayed(run, 1000);
    }

  /*  private void initializeList(){

        ringtones = RingtoneHelper.fetchAvailableRingtones(this.getActivity());
        ArrayAdapter<Ringtone> adapter = new ArrayAdapter<Ringtone>(getActivity(),
                android.R.layout.simple_list_item_1, ringtones) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView item = (TextView)super.getView(position, convertView, parent);
                item.setText(ringtones.get(position).getTitle(getActivity()));
                return item;
            }
        };

        listOfRingtones.setAdapter(adapter);
    }*/

}