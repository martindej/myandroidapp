package musicRandom;

public class instrument {
	
	int num_instrument;
	int inst_ref;
	int instruments;
	int rythme_max;
	int note_ref;
	int note_min;
	int note_max;
	int tonalite;
	int rythme;
	int sol_basse;
	int nbre_accords;
	int inc;
	int inc_2;
	
	musique pt;
	motif mot;
	
	int [][] rythmes_notes_not; 
	int [] compt;
	float [][][] parti;
	int [][] accords;
	int [][] mot_acc;
	
	instrument(int Inum_instrument,int Iinst_ref,int Iinstruments, int Irythme_max,int Inote_ref, int Inote_min, int Inote_max, int Itonalite, int Irythme,int Isol_basse,int Inbre_accords, musique Ipt, motif Imot)
	{
		pt = Ipt;
		mot = Imot;
		
		num_instrument = Inum_instrument;
		inst_ref = Iinst_ref;
		instruments = Iinstruments;
		rythme_max = Irythme_max;
		note_ref = Inote_ref;
		note_min = Inote_min;
		note_max = Inote_max;
		tonalite = Itonalite;
		rythme = Irythme;
		sol_basse = Isol_basse;
		nbre_accords = Inbre_accords;
	
		rythmes_notes_not = new int [5][12]; // la derniere case correspond au pourcentage de silence
		compt = new int [5];
		parti = new float [5][10][16*nbre_accords*pt.get_duree_accords()];
		accords = new int [5][nbre_accords];
	
		accords = mot.get_accords();
	
		mot_acc = new int [6][pt.get_duree_accords()*4];
	
		inc = 0;
		inc_2 = 0;
	}
	

	void creation_partie(int maj_min)
	{
		int a_get,d_accords,valeur_voie,i, partie,num_accord_courant,somme=0;
	
		d_accords = pt.get_duree_accords();
		a_get = pt.get_a(num_instrument);
	
		do
		{
			a_get = pt.get_a(num_instrument);
	
			if(a_get == 0 && accords[2][0] == 1)
				inc++;
	
			choix_rythmes(inc);
			rythmes_notes_not[inc][8] = (int)( Math.random()*40 );  // pourcentage de silences
			rythmes_notes_not[inc][9] = (int)( Math.random()*3 ) - 1;  // musique descendante / neutre / ascendante
			rythmes_notes_not[inc][10] = (int)( Math.random()*13 );  // ecart entre note a privilegier
			rythmes_notes_not[inc][11] = (int)( Math.random()*3 ) + 2;  // nombre de notes dans l'accord de l'instrument
	
	
			num_accord_courant = (int)(a_get/(16*d_accords));
	
			if(inc != accords[2][num_accord_courant])
			{
				pt.add_parti(parti[accords[2][num_accord_courant]],compt[accords[2][num_accord_courant]] ,num_instrument); // accords[4][num_accord_courant]*16*d_accords
				a_get = pt.get_a(num_instrument);
	
				for(i=0;i<9;i++)
				{
					rythmes_notes_not[inc][i] = rythmes_notes_not[accords[2][num_accord_courant]][i];
				}
				somme = somme + accords[4][num_accord_courant];
			}
	
			num_accord_courant = (int)(a_get/(16*d_accords));
	
			if (inc == accords[2][num_accord_courant])
			{
				somme = somme + accords[4][num_accord_courant];
				partie = 1;
				do
				{
	
					valeur_voie = pt.valeur_vect_voies(inst_ref,(int)(a_get/(16*d_accords)));
	
					if((valeur_voie == 3) && ((a_get%(16*d_accords)) == 0))
					{
						inc_2 = 0;
					}
	
					note note_a = new note(tonalite,accords[0][(int)(a_get/(16*d_accords))],accords[1][(int)(a_get/(16*d_accords))],accords[2][(int)(a_get/(16*d_accords))],accords[3][(int)(a_get/(16*d_accords))],maj_min,num_instrument,valeur_voie,note_ref, note_min, note_max, inc_2,partie,sol_basse,instruments, pt,mot,this);
	
					note_a.creer();
					a_get = pt.get_a(num_instrument);
				}
				while(a_get<(somme*(16*d_accords)) );
	
				inc++;
			}
	
			num_accord_courant = (int)(a_get/(16*d_accords));
	
			if( accords[2][num_accord_courant-1] != 0 )
			{
				somme = somme + accords[4][num_accord_courant];
	
				partie = 2;
				do
				{
					valeur_voie = pt.valeur_vect_voies(inst_ref,(int)(a_get/(16*d_accords)));
	
					if((valeur_voie == 3) && ((a_get%(16*d_accords)) == 0))
					{
						inc_2 = 0;
					}
	
					note note_a = new note(tonalite,accords[0][(int)(a_get/(16*d_accords))],accords[1][(int)(a_get/(16*d_accords))],accords[2][(int)(a_get/(16*d_accords))],accords[3][(int)(a_get/(16*d_accords))],maj_min,num_instrument,valeur_voie,note_ref, note_min, note_max, inc_2,partie,sol_basse,instruments, pt,mot,this);
					note_a.creer();
					a_get = pt.get_a(num_instrument);
				}
				while(a_get<(somme*(16*d_accords)));
	
			}
		}
		while(a_get<(nbre_accords*16*d_accords) );
	}
	
	int[] get_rythmes_not(int t)
	{
		return rythmes_notes_not[t];
	}

	void choix_rythmes(int t)
	{
//		cout << "function choix_rythmes "<< endl;
		int mauvais = 0, duree_note,test = 0,j,trois = 0,quatre = 0,i,six=0;

		rythmes_notes_not[t][0] = (int)( Math.random()*3 ) + 1;   //nbre rythmes not

		for(i=1;i<rythmes_notes_not[t][0]+1 ;i++)
		{	
			do
			{
				mauvais = 0;
				duree_note = (int)( Math.random()*7 ) + 3 ; 

				if(i>1)
				{
					for(j=1;j<i;j++)
					{
						if(duree_note == rythmes_notes_not[t][j])
							mauvais = 1;
					}
				}
				if(mauvais == 0)
					rythmes_notes_not[t][i] = duree_note;
			}
			while(mauvais == 1);
		}
		if(rythme *4 > rythme_max)
		{
			test = 0;
			for(i=1;i<rythmes_notes_not[t][0]+1;i++)
			{
				if(rythmes_notes_not[t][i] == 3)
					test = 1;
			}
			if(test == 0)
			{
				rythmes_notes_not[t][rythmes_notes_not[t][0]+1] = 3;
				rythmes_notes_not[t][0] = rythmes_notes_not[t][0] + 1;
			}
		}
		if(rythme *2 > rythme_max)
		{
			test = 0;
			for(i=1;i<rythmes_notes_not[t][0]+1;i++)
			{
				if(rythmes_notes_not[t][i] == 4)
					test = 1;
			}
			if(test == 0)
			{
				rythmes_notes_not[t][rythmes_notes_not[t][0]+1] = 4;
				rythmes_notes_not[t][0] = rythmes_notes_not[t][0] + 1;
			}
		}
		if(rythme > rythme_max)
		{
			test = 0;
			for(i=1;i<rythmes_notes_not[t][0]+1;i++)
			{
				if(rythmes_notes_not[t][i] == 5)
					test = 1;
			}
			if(test == 0)
			{
				rythmes_notes_not[t][rythmes_notes_not[t][0]+1] = 5;
				rythmes_notes_not[t][0] = rythmes_notes_not[t][0] + 1;
			}
		}
		if(rythme/1.5 > rythme_max)
		{
			test = 0;
			for(i=1;i<rythmes_notes_not[t][0]+1;i++)
			{
				if(rythmes_notes_not[t][i] == 6)
					test = 1;
			}
			if(test == 0)
			{
				rythmes_notes_not[t][rythmes_notes_not[t][0]+1] = 6;
				rythmes_notes_not[t][0] = rythmes_notes_not[t][0] + 1;
			}
		}
		if(rythme/2 > rythme_max)
		{
			test = 0;
			for(i=1;i<rythmes_notes_not[t][0]+1;i++)
			{
				if(rythmes_notes_not[t][i] == 7)
					test = 1;
			}
			if(test == 0)
			{
				rythmes_notes_not[t][rythmes_notes_not[t][0]+1] = 7;
				rythmes_notes_not[t][0] = rythmes_notes_not[t][0] + 1;
			}
		}
		if(rythme/3 > rythme_max)
		{
			test = 0;
			for(i=1;i<rythmes_notes_not[t][0]+1;i++)
			{
				if(rythmes_notes_not[t][i] == 8)
					test = 1;
			}
			if(test == 0)
			{
				rythmes_notes_not[t][rythmes_notes_not[t][0]+1] = 8;
				rythmes_notes_not[t][0] = rythmes_notes_not[t][0] + 1;
			}
		}
		for(i=1;i<rythmes_notes_not[t][0]+1;i++)
		{
			if(rythmes_notes_not[t][i] == 3)
				trois = 1;
			if(rythmes_notes_not[t][i] == 4)
				quatre = 1;
			if(rythmes_notes_not[t][i] == 6)
				six = 1;
		}
		if (trois == 1 && quatre == 1 && six == 0)
		{
			if(rythmes_notes_not[t][0] > 2)
			{
				i = 0;
				do
				{
					i++;
						
				}
				while((rythmes_notes_not[t][i] != 3 && rythmes_notes_not[t][i] != 4) || i > 8);
				
				rythmes_notes_not[t][i] = 6;
			}
			else
			{
				rythmes_notes_not[t][0] = rythmes_notes_not[t][0] + 1;
				rythmes_notes_not[t][3] = 6;
			}
		}
		if(rythme/4 > rythme_max)
		{
			System.out.println("rythme trop rapide pour cet instrument");
		}	
	}

	void set_partition(int m,int oct, int no,int duree_note, int sil,int volume,int note_1,int note_2,int note_3)
	{
		int u = 0, duree_note_1 = 0;

		if(duree_note == 3)
			duree_note_1 = 4;
		if(duree_note == 4)
			duree_note_1 = 8;
		if(duree_note == 5)
			duree_note_1 = 16;
		if(duree_note == 6)
			duree_note_1 = 24;
		if(duree_note == 7)
			duree_note_1 = 32;
		if(duree_note == 8)
			duree_note_1 = 48;
		if(duree_note == 9)
			duree_note_1 = 64;

		duree_note = duree_note_1;

		for (u=compt[m];u<compt[m]+duree_note;u++)
		{
			parti[m][0][u] = sil;          //silence
			parti[m][1][u] = oct;	        //octave
			parti[m][2][u] = no;           //num note
			parti[m][3][u] = 0;            //indicateur fin duree note
			parti[m][4][u] = volume;            //volume
			parti[m][5][u] = note_1;           // note supplémentaires accords 
			parti[m][6][u] = note_2;
			parti[m][7][u] = note_3;
			parti[m][8][u] = 1;
			parti[m][9][u] = 1;            //details
		}
		parti[m][3][compt[m]+duree_note-1] = 1;

		compt[m] = u;
	}

	int get_moti(int ligne, int colonne)
	{
		return mot_acc[ligne][colonne];
	}


	void set_moti(int num_note, int ryth, int joue,int note_1,int note_2,int note_3)
	{
//		cout << "function set_moti "<< endl;
		mot_acc[0][inc_2] = num_note;
		mot_acc[1][inc_2] = ryth;
		mot_acc[2][inc_2] = joue;
		mot_acc[3][inc_2] = note_1;
		mot_acc[4][inc_2] = note_2;
		mot_acc[5][inc_2] = note_3;

		inc_2++;
	}

}
