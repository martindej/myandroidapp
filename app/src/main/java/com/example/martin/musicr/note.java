package musicRandom;

public class note {
	
	int tonalite;
	int degre;
	int num_accord;
	int part;
	int repere;
	int maj_min;
	int num_instrument;
	int moti;
	int note_ref;
	int note_min;
	int note_max;
	int inc_2;
	int partie;
	int sol_basse;
	int instruments;
	int d_accords;
	int pourc_sil;
	int tonale;
	int pos_neg;
	int somme;
	int no;
	int oct;
	int num_note;
	int duree_note;
	
	musique pt;
	motif mot;
	instrument in;
	
	int [] rythmes_not;
	int [] derniere_note;
	int [] vect;
	int [] vect_accord;
	int [] note_;
	
	note(int Ntonalite,int Ndegre, int Nnum_accord,int Npart,int Nrepere, int Nmaj_min, int Nnum_instrument,int Nmoti,int Nnote_ref, int Nnote_min, int Nnote_max, int Ninc_2,int Npartie,int Nsol_basse,int Ninstruments, musique Npt, motif Nmot,instrument Nin)
	{
		pt = Npt;
		mot = Nmot;
		in = Nin;
		
		tonalite = Ntonalite;
		degre = Ndegre;
		num_accord = Nnum_accord;
		part = Npart;
		repere = Nrepere;
		maj_min = Nmaj_min;
		num_instrument = Nnum_instrument;
		moti = Nmoti;
		note_ref = Nnote_ref;
		note_min = Nnote_min;
		note_max = Nnote_max;
		inc_2 = Ninc_2;
		partie = Npartie;
		sol_basse = Nsol_basse;
		instruments = Ninstruments;
	
		d_accords = pt.get_duree_accords();
		rythmes_not = new int [12];
		derniere_note = new int [10];
		rythmes_not = in.get_rythmes_not(part);
		pourc_sil = rythmes_not[8];
	}
	
	void creer()  
	{
//		cout << "function creer "<< endl;
		int a = 0,b,c,i, pourc,duree_note_1 = 0, oct_pre, no_pre, num_note_pre = 0, ecart_1,test,joue_or_not,volume = 0,nbre_not_ac = 0,num_note_test,oct_1,no_1,j,k, tboucle=0;

		if(sol_basse == 0)
			volume = 127;
		if(sol_basse == 1)
			volume = 100;

		vect = new int [72];            //tableau des notes de la tonalit� 
		vect_accord = new int [12];      // tableau des notes de l'accord
		note_ = new int [4];           // tableau des notes jou�es en meme temps

		vect_tonalite();            // fonction de remplissage du vecteur de tonalit�
		tonale = det_tonale();        // determination de la tonale 
		fct_vect_accord();         // fonction de remplissage du vecteur de l'accord

		a = pt.get_a(num_instrument);       // a = curseur de la place de la note actuelle 
		b = a/16;  // b = curseur du temps precedent 
		c = (b+1)*16 - a;  // c = temps restant avant le prochain temps

		/* moti = 2 :  rythme identique, note diff�rente*/
		if(moti == 2)
		{
			if( (((int)((a/16)/pt.get_duree_accords())+1)*pt.get_duree_accords()*16 - a) == pt.get_duree_accords()*16)
			{
				for(i=0;i<inc_2;i++)
				{
					note_ = new int [4];

					if(a==0)
					{
						num_note_pre = note_ref;
					}
					if(a!=0)
					{
						derniere_note = pt.get_note(a-1,num_instrument);

						oct_pre = derniere_note[1];
						no_pre = derniere_note[2];
						num_note_pre = oct_pre*12 + no_pre;
					}

					if(c == 16)
					{
						det_note(num_note_pre,1);

						note_[0] = num_note;

						for(j=3;j<6;j++)
						{
							if(in.get_moti(j,i) != 0)
							{
								tboucle = 0;
								do
								{
									test = 0;
									tboucle++;
									det_note(num_note_pre,2);

									for(k=0;k<j-2;k++)
									{
										if(num_note == note_[k])
											test = 1;
									}
									if(tboucle>1000)
									{
										test = 0;
										j++;
									}
									
								}
								while(test == 1); // vect_accord[no_1] != 1 || (num_note > note_max) || (num_note < note_min) || (

								note_[j-2] = num_note;
							}
						}
					}
					else
					{
						det_note(num_note_pre,0);

						note_[0] = num_note;

						for(j=3;j<6;j++)
						{
							if(in.get_moti(j,i) != 0)
							{
								tboucle=0;
								do
								{
									test = 0;
									tboucle++;
									det_note(num_note_pre,3);
									
									for(k=0;k<j-2;k++)
									{
										if(num_note == note_[k])
											test = 1;
									}
									if(tboucle>1000)
									{
										System.out.println("erreur test5 "+num_note+" "+test);
									}

								}
								while(test == 1); // vect[num_note] != 1 || (num_note > note_max) || (num_note < note_min) || (

								note_[j-2] = num_note;
							}
						}
					}
					duree_note = in.get_moti(1,i);
					joue_or_not = in.get_moti(2,i);

					pt.set_partition(oct,no,duree_note,num_instrument,joue_or_not,volume,note_[1],note_[2],note_[3]);
					if(partie == 1)
					{
						in.set_partition(part,oct,no,duree_note,joue_or_not,volume,note_[1],note_[2],note_[3]);
					}
					a = pt.get_a(num_instrument);
				}
			}
			else
			{
				moti = 1;
			}
		}

		if(moti == 4)
		{
			if( (((int)((a/16)/pt.get_duree_accords())+1)*pt.get_duree_accords()*16 - a) == pt.get_duree_accords()*16)
			{
				for(i=0;i<inc_2;i++)
				{
					num_note = in.get_moti(0,i);
					duree_note = in.get_moti(1,i);
					joue_or_not = in.get_moti(2,i);

					oct = num_note/12;
					no = num_note%12;

					pt.set_partition(oct,no,duree_note,num_instrument,joue_or_not,volume,note_[1],note_[2],note_[3]);
					if(partie == 1)
					{
						in.set_partition(part,oct,no,duree_note,joue_or_not,volume,note_[1],note_[2],note_[3]);
					}
					a = pt.get_a(num_instrument);
				}
			}
			else
			{
				moti = 1;
			}
		}
				
				
		if(moti == 1 || moti == 3)
		{
			if(a!=0)
			{
				derniere_note = pt.get_note(a-1,num_instrument);
				//duree_derniere_note = pt->get_duree_note(a-1,num_instrument);

				oct_pre = derniere_note[1];
				no_pre = derniere_note[2];
				num_note_pre = oct_pre*12 + no_pre;

				if(c == 16)
				{
					det_note(num_note_pre,1);

					duree_note = det_duree_note();
				}
				else
				{
					if(c == 12)
					{
						do
						{				
							duree_note = (int)( Math.random()*10);
							test = 0;

							if(duree_note > 0)
							{
								duree_note_1 = 4;
								duree_note = 3;
							}
							if(duree_note == 0)
							{
								duree_note_1 = 8;
								duree_note = 4;
							}
							if(tboucle<1000)
							{
								for(i=1;i<rythmes_not[0]+1;i++)
								{
									if(duree_note == rythmes_not[i])
										test = 1;
								}
							}

						}
						while(test == 1);
					}
					else
					{
						do
						{
							duree_note = (int)( Math.random()*2 );
							test = 0;

							if(duree_note == 0)
							{
								duree_note_1 = 4;
								duree_note = 3;
							}
							if(duree_note == 1)
							{
								duree_note_1 = 8;
								duree_note = 4;
							}


							if(tboucle<1000)
							{
								for(i=1;i<rythmes_not[0]+1;i++)
								{
									if(duree_note == rythmes_not[i])
										test = 1;
								}
							}

						}
						while(duree_note_1 > c || test == 1);	
					}

					if(duree_note_1 == c)
					{
						det_note(num_note_pre,0);
					}
					else
					{
						det_note(num_note_pre,1);
					}
			
				}
			}
			if ( a == 0 )
			{
				det_note(note_ref,0);
				duree_note = det_duree_note();
			}

			note_[0] = num_note;
			
			if(instruments == 200)
			{
				note_[0] = (int)( Math.random()*57 ) + 27;
			}

			pourc_sil = (int)(pourc_sil - (duree_note - 3)*(pourc_sil/6.0));
			
			if(derniere_note[0] == 0)
				pourc_sil = (int)(pourc_sil/10.0);

			pourc = (int)( Math.random()*101 );

			// pour les instruments pouvant jouer des accords, on choisit le nombre de notes de l'accord et on choisit les autres notes

			if(instruments < 40 || instruments > 127)
			{

				if(instruments < 40 )
				{
					// nbre de notes fixe, soit 1 note, soit accord, le choix est fait selon un probabilité dépendant de la duree de la note

					nbre_not_ac = (int)( Math.random()*10 );
					if(nbre_not_ac > duree_note)
					{
						nbre_not_ac = rythmes_not[11];
					}
					else
					{
						nbre_not_ac = 1;
					}
				}

				if(instruments > 127)
				{

					nbre_not_ac = (int)( Math.random()*10 ) + 3;
					if(nbre_not_ac > duree_note)
					{
						nbre_not_ac = rythmes_not[11];
					}
					else
					{
						nbre_not_ac = 1;
					}
				}

				if(a==0)
				{
					num_note_pre = note_ref;
				}
				
				if(nbre_not_ac > somme)
				{
					nbre_not_ac = somme;
				}

				if(nbre_not_ac > 1)
				{
					for(i=1;i<nbre_not_ac;i++)
					{
						do
						{
							test = 0;
							det_note(num_note_pre,2);
							
							for(j=0;j<i;j++)
							{
								if(num_note == note_[j])
									test = 1;
							}

						}
						while(test == 1); //vect_accord[no_1] != 1 || (num_note > note_max) || (num_note < note_min) || (

						note_[i] = num_note;
					}
				}
			}

			if(pourc < pourc_sil)
			{
				pt.set_partition(oct,no,duree_note,num_instrument,0,volume,note_[1],note_[2],note_[3]);
				if(partie == 1)
				{
					in.set_partition(part,oct,no,duree_note,0,volume,note_[1],note_[2],note_[3]);
				}
			}
			if(pourc >= pourc_sil)
			{
				pt.set_partition(oct,no,duree_note,num_instrument,1,volume,note_[1],note_[2],note_[3]);
				if(partie == 1)
				{
					in.set_partition(part,oct,no,duree_note,1,volume,note_[1],note_[2],note_[3]);
				}
			}

			if(moti == 3 && (pourc < pourc_sil))
			{
				in.set_moti(oct*12 + no,duree_note,0,note_[1],note_[2],note_[3]);
			}
			if(moti == 3 && (pourc >= pourc_sil))
			{
				in.set_moti(oct*12 + no,duree_note,1,note_[1],note_[2],note_[3]);
			}
		}
			
		if(moti == 0)
		{
			a = pt.get_a(num_instrument);
			duree_note = ((int)((a/16)/pt.get_duree_accords())+1)*pt.get_duree_accords()*16 - a;
			oct = note_ref/12;
			no = note_ref%12;

			do
			{
				if(duree_note >= 64)
				{
					pt.set_partition(oct,no,9,num_instrument,0,volume,note_[1],note_[2],note_[3]);
					if(partie == 1)
					{
						in.set_partition(part,oct,no,9,0,volume,note_[1],note_[2],note_[3]);
					}
					duree_note = duree_note - 64;
				}
				if(duree_note >= 48 && duree_note < 64)
				{
					pt.set_partition(oct,no,8,num_instrument,0,volume,note_[1],note_[2],note_[3]);
					if(partie == 1)
					{
						in.set_partition(part,oct,no,8,0,volume,note_[1],note_[2],note_[3]);
					}
					duree_note = duree_note - 48;
				}
				if(duree_note >= 32 && duree_note < 48)
				{
					pt.set_partition(oct,no,7,num_instrument,0,volume,note_[1],note_[2],note_[3]);
					if(partie == 1)
					{
						in.set_partition(part,oct,no,7,0,volume,note_[1],note_[2],note_[3]);
					}
					duree_note = duree_note - 32;
				}
				if(duree_note >= 24 && duree_note < 32)
				{
					pt.set_partition(oct,no,6,num_instrument,0,volume,note_[1],note_[2],note_[3]);
					if(partie == 1)
					{
						in.set_partition(part,oct,no,6,0,volume,note_[1],note_[2],note_[3]);
					}
					duree_note = duree_note - 24;
				}
				if(duree_note >= 16 && duree_note < 24)
				{
					pt.set_partition(oct,no,5,num_instrument,0,volume,note_[1],note_[2],note_[3]);
					if(partie == 1)
					{
						in.set_partition(part,oct,no,5,0,volume,note_[1],note_[2],note_[3]);
					}
					duree_note = duree_note - 16;
				}
				if(duree_note >= 8 && duree_note < 16)
				{
					pt.set_partition(oct,no,4,num_instrument,0,volume,note_[1],note_[2],note_[3]);
					if(partie == 1)
					{
						in.set_partition(part,oct,no,4,0,volume,note_[1],note_[2],note_[3]);
					}
					duree_note = duree_note - 8;
				}
				if(duree_note >= 4 && duree_note < 8)
				{
					pt.set_partition(oct,no,3,num_instrument,0,volume,note_[1],note_[2],note_[3]);
					if(partie == 1)
					{
						in.set_partition(part,oct,no,3,0,volume,note_[1],note_[2],note_[3]);
					}
					duree_note = duree_note - 4;
				}
			}
			while(duree_note != 0);
		}

	}

	void det_note(int num_note_pre, int accord_or_not)
	{
		int ecart_1,oct_1,no_1;

		if(accord_or_not == 1)
		{
			do
			{
				det_pos_neg(num_note_pre);
		
				ecart_1 = det_ecart();
				ecart_1 = ecart_1*pos_neg;
				num_note = num_note_pre + ecart_1;
		
				oct = (num_note_pre + ecart_1)/12;
				no = (num_note_pre + ecart_1)%12;
				if(no <0)
				{
					no = 0;
				}
			}
			while(vect_accord[no] != 1 || (num_note > note_max) || (num_note < note_min));
		}
		else if(accord_or_not == 2)
		{
			do
			{
				det_pos_neg(num_note_pre);
		
				ecart_1 = det_ecart();
				ecart_1 = ecart_1*pos_neg;
				num_note = num_note_pre + ecart_1;
		
				oct_1 = (num_note_pre + ecart_1)/12;
				no_1 = (num_note_pre + ecart_1)%12;
				
				if(no_1 <0)
				{
					no_1 = 0;
				}
			}
			while(vect_accord[no_1] != 1 || (num_note > note_max) || (num_note < note_min));
		}
		else if(accord_or_not == 3)
		{
			do
			{
				
				det_pos_neg(num_note_pre);
		
				ecart_1 = det_ecart();
				ecart_1 = ecart_1*pos_neg;
				num_note = num_note_pre + ecart_1;
		
				oct_1 = (num_note_pre + ecart_1)/12;
				no_1 = (num_note_pre + ecart_1)%12;
				
				if(num_note <0)
				{
					num_note = 0;
				}
				if(num_note >71)
				{
					num_note = 71;
				}
			}
			while(vect[num_note] != 1 || (num_note > note_max) || (num_note < note_min));
		}
		else
		{
			do
			{
				
				det_pos_neg(num_note_pre);

				ecart_1 = det_ecart();
				ecart_1 = ecart_1*pos_neg;
				num_note = num_note_pre + ecart_1;

				oct = (num_note_pre + ecart_1)/12;
				no = (num_note_pre + ecart_1)%12;
				
				if(num_note <0)
				{
					num_note = 0;
				}
				if(num_note >71)
				{
					num_note = 71;
				}
			}
			while(vect[num_note] != 1 || (num_note > note_max) || (num_note < note_min));
		}
		
	}

	int det_duree_note()
	{
		int duree_note_1, duree_note = 0,i,test, compt;	

		compt = 0;

		do
		{
			test = 0;
			compt++;
			duree_note_1 = (int)( Math.random()*18 ) + 3;

			if(duree_note_1 == 10)
				duree_note = 7;
			if(duree_note_1 == 11)
				duree_note = 3;
			if(duree_note_1>11 && duree_note_1<15)
				duree_note = 5;
			if(duree_note_1>14 && duree_note_1<18)
				duree_note = 4;
			if(duree_note_1>17 && duree_note_1<21)
				duree_note = 6;
			if(duree_note_1<10)
				duree_note = duree_note_1;

			test = verif_duree_note_accords(duree_note);

			if(compt < 1000)
			{
				for(i=1;i<rythmes_not[0]+1;i++)
				{

					if(duree_note == rythmes_not[i])
						test = 1;
				}
			}
		}
		while(test == 1);

		return duree_note;
	}

	int verif_duree_note_accords(int duree_note)
	{

		int duree_note_1 = 0,test,a;
		a = pt.get_a(num_instrument);

		if(duree_note == 3)
			duree_note_1 = 4;
		if(duree_note == 4)
			duree_note_1 = 8;
		if(duree_note == 5)
			duree_note_1 = 16;
		if(duree_note == 6)
			duree_note_1 = 24;
		if(duree_note == 7)
			duree_note_1 = 32;
		if(duree_note == 8)
			duree_note_1 = 48;
		if(duree_note == 9)
			duree_note_1 = 64;

		if(((repere==5)||(repere==8))&&((a+duree_note_1)>(((int)((a/16)/pt.get_duree_accords())+1)*pt.get_duree_accords()*16)))
		{
				return 1;
		}

		if((duree_note_1 > ((int)((a/16)/pt.get_duree_accords())+1)*pt.get_duree_accords()*16 - a) && moti == 1)
		{
			test = (int)( Math.random()*10 );
			if(test == 0)
				return 0;
			else
				return 1;
		}
		if((duree_note_1 > ((int)((a/16)/pt.get_duree_accords())+1)*pt.get_duree_accords()*16 - a) && (moti == 2 || moti == 3) )
		{
			return 1;
		}
		return 0;
	}

	int det_ecart()
	{
		int ecart, ecart_1 = 0;

		ecart = (int)( Math.random()*122 ) + 7;

		if(ecart<10)
			ecart_1 = 0;
		if(ecart>9 && ecart<23)
			ecart_1 = 1;
		if(ecart>22 && ecart<39)
			ecart_1 = 2;
		if(ecart>38 && ecart<59)
			ecart_1 = 3;
		if(ecart>58 && ecart<77)
			ecart_1 = 4;
		if(ecart>76 && ecart<91)
			ecart_1 = 5;
		if(ecart>90 && ecart<101)
			ecart_1 = 6;
		if(ecart>100 && ecart<108)
			ecart_1 = 7;
		if(ecart>107 && ecart<112)
			ecart_1 = 8;
		if(ecart>111 && ecart<115)
			ecart_1 = 9;
		if(ecart>114 && ecart<117)
			ecart_1 = 10;
		if(ecart == 117)
			ecart_1 = 11;
		if(ecart == 118)
			ecart_1 = 12;
		if(ecart>118 && ecart<129)
			ecart_1 = rythmes_not[10];

		return ecart_1;
	}

	void det_pos_neg(int num_note_pre)
	{
		int pos_neg_1;

		if((num_note_pre-note_ref)==0)
		{
			pos_neg_1 = (int)( Math.random()*2 );
			if(pos_neg_1==0)
				pos_neg = -1;
			if(pos_neg_1==1)
				pos_neg = 1;
		}
			
		if((num_note_pre-note_ref)>0 &&  (num_note_pre-note_ref)<3)
		{
			pos_neg_1 = (int)( Math.random()*7 );
			if(pos_neg_1<4)
				pos_neg = -1;
			if(pos_neg_1>3)
				pos_neg = 1;
		}
		if((num_note_pre-note_ref)>0 &&  (num_note_pre-note_ref)<6 &&  (num_note_pre-note_ref)>2)
		{
			pos_neg_1 = (int)( Math.random()*5 );
			if(pos_neg_1<3)
				pos_neg = -1;
			if(pos_neg_1>2)
				pos_neg = 1;
		}
		if((num_note_pre-note_ref)>0 &&  (num_note_pre-note_ref)<13 &&  (num_note_pre-note_ref)>5)
		{
			pos_neg_1 = (int)( Math.random()*3 );
			if(pos_neg_1<2)
				pos_neg = -1;
			if(pos_neg_1>1)
				pos_neg = 1;
		}
		if((num_note_pre-note_ref)>0 &&  (num_note_pre-note_ref)<17 &&  (num_note_pre-note_ref)>12)
		{
			pos_neg_1 = (int)( Math.random()*4 );
			if(pos_neg_1<3)
				pos_neg = -1;
			if(pos_neg_1==3)
				pos_neg = 1;
		}
		if((num_note_pre-note_ref)>0 &&  (num_note_pre-note_ref)>16)
		{
			pos_neg_1 = (int)( Math.random()*10 );
			if(pos_neg_1<9)
				pos_neg = -1;
			if(pos_neg_1==9)
				pos_neg = 1;
		}

		if((num_note_pre-note_ref)<0 &&  Math.abs(num_note_pre-note_ref)<3)
		{
			pos_neg_1 = (int)( Math.random()*7 );
			if(pos_neg_1<4)
				pos_neg = 1;
			if(pos_neg_1>3)
				pos_neg = -1;
		}
		if((num_note_pre-note_ref)<0 &&  Math.abs(num_note_pre-note_ref)<6 &&  Math.abs(num_note_pre-note_ref)>2)
		{
			pos_neg_1 = (int)( Math.random()*5 );
			if(pos_neg_1<3)
				pos_neg = 1;
			if(pos_neg_1>2)
				pos_neg = -1;
		}
		if((num_note_pre-note_ref)<0 &&  Math.abs(num_note_pre-note_ref)<13 &&  Math.abs(num_note_pre-note_ref)>5)
		{
			pos_neg_1 = (int)( Math.random()*3 );
			if(pos_neg_1<2)
				pos_neg = 1;
			if(pos_neg_1>1)
				pos_neg = -1;
		}
		if((num_note_pre-note_ref)<0 &&  Math.abs(num_note_pre-note_ref)<17 &&  Math.abs(num_note_pre-note_ref)>12)
		{
			pos_neg_1 = (int)( Math.random()*4);
			if(pos_neg_1<3)
				pos_neg = 1;
			if(pos_neg_1==3)
				pos_neg = -1;
		}
		if((num_note_pre-note_ref)<0 &&  Math.abs(num_note_pre-note_ref)>16)
		{
			pos_neg_1 = (int)( Math.random()*10 );
			if(pos_neg_1<9)
				pos_neg = 1;
			if(pos_neg_1==9)
				pos_neg = -1;
		}
	}

	int det_tonale()
	{
		if(maj_min == 1)
		{
			switch(tonalite)
			{
				case 1:
					return 2;
				case 2:
					return 7;
				case 3:
					return 12;
				case 4:
					return 5;
				case 5:
					return 10;
				case 6:
					return 3;
				case 7:
					return 8;
				case 8:
					return 1;
				case 9:
					return 6;
				case 10:
					return 11;
				case 11:
					return 4;
				case 12:
					return 9;
				case 13:
					return 2;
				case 14:
					return 7;
				case 15:
					return 12;
			}
		}
		if (maj_min == 2)
		{
			switch(tonalite)
			{
				case 1:
					return 11;
				case 2:
					return 4;
				case 3:
					return 9;
				case 4:
					return 2;
				case 5:
					return 7;
				case 6:
					return 12;
				case 7:
					return 5;
				case 8:
					return 10;
				case 9:
					return 3;
				case 10:
					return 8;
				case 11:
					return 1;
				case 12:
					return 6;
				case 13:
					return 11;
				case 14:
					return 4;
				case 15:
					return 9;
			}
		}

		return 0;
	}

	void vect_tonalite()
	{
		int i;

		if(maj_min == 1)
		{
			switch(tonalite)
			{
				case 1:
					vect[0] = 1;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 0;
					vect[5] = 1;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 0;
					break;

				case 2:
					vect[0] = 0;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 0;
					vect[5] = 1;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 1;
					break;

				case 3:
					vect[0] = 0;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 1;
					vect[5] = 0;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 1;
					break;

				case 4:
					vect[0] = 0;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 1;
					vect[5] = 0;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 1;
					vect[9] = 1;
					vect[10] = 0;
					vect[11] = 1;
					break;

				case 5:
					vect[0] = 0;
					vect[1] = 1;
					vect[2] = 1;
					vect[3] = 0;
					vect[4] = 1;
					vect[5] = 0;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 1;
					vect[9] = 1;
					vect[10] = 0;
					vect[11] = 1;
					break;

				case 6:
					vect[0] = 0;
					vect[1] = 1;
					vect[2] = 1;
					vect[3] = 0;
					vect[4] = 1;
					vect[5] = 0;
					vect[6] = 1;
					vect[7] = 1;
					vect[8] = 0;
					vect[9] = 1;
					vect[10] = 0;
					vect[11] = 1;
					break;

				case 7:
					vect[0] = 1;
					vect[1] = 0;
					vect[2] = 1;
					vect[3] = 0;
					vect[4] = 1;
					vect[5] = 0;
					vect[6] = 1;
					vect[7] = 1;
					vect[8] = 0;
					vect[9] = 1;
					vect[10] = 0;
					vect[11] = 1;
					break;

				case 8:
					vect[0] = 1;
					vect[1] = 0;
					vect[2] = 1;
					vect[3] = 0;
					vect[4] = 1;
					vect[5] = 1;
					vect[6] = 0;
					vect[7] = 1;
					vect[8] = 0;
					vect[9] = 1;
					vect[10] = 0;
					vect[11] = 1;
					break;

				case 9:
					vect[0] = 1;
					vect[1] = 0;
					vect[2] = 1;
					vect[3] = 0;
					vect[4] = 1;
					vect[5] = 1;
					vect[6] = 0;
					vect[7] = 1;
					vect[8] = 0;
					vect[9] = 1;
					vect[10] = 1;
					vect[11] = 0;
					break;

				case 10:
					vect[0] = 1;
					vect[1] = 0;
					vect[2] = 1;
					vect[3] = 1;
					vect[4] = 0;
					vect[5] = 1;
					vect[6] = 0;
					vect[7] = 1;
					vect[8] = 0;
					vect[9] = 1;
					vect[10] = 1;
					vect[11] = 0;
					break;

				case 11:
					vect[0] = 1;
					vect[1] = 0;
					vect[2] = 1;
					vect[3] = 1;
					vect[4] = 0;
					vect[5] = 1;
					vect[6] = 0;
					vect[7] = 1;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 0;
					break;

				case 12:
					vect[0] = 1;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 0;
					vect[5] = 1;
					vect[6] = 0;
					vect[7] = 1;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 0;
					break;

				case 13:
					vect[0] = 1;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 0;
					vect[5] = 1;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 0;
					break;

				case 14:
					vect[0] = 0;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 0;
					vect[5] = 1;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 1;
					break;

				case 15:
					vect[0] = 0;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 1;
					vect[5] = 0;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 1;
					break;
			}
		}
		if(maj_min == 2)
		{
			switch(tonalite)
			{
				case 1:
					vect[0] = 1;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 0;
					vect[5] = 1;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 0;
					vect[9] = 1;
					vect[10] = 1;
					vect[11] = 0;
					break;

				case 2:
					vect[0] = 0;
					vect[1] = 0;
					vect[2] = 1;
					vect[3] = 1;
					vect[4] = 0;
					vect[5] = 1;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 1;
					break;

				case 3:
					vect[0] = 0;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 1;
					vect[5] = 0;
					vect[6] = 0;
					vect[7] = 1;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 1;
					break;

				case 4:
					vect[0] = 1;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 1;
					vect[5] = 0;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 1;
					vect[9] = 1;
					vect[10] = 0;
					vect[11] = 0;
					break;

				case 5:
					vect[0] = 0;
					vect[1] = 1;
					vect[2] = 1;
					vect[3] = 0;
					vect[4] = 0;
					vect[5] = 1;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 1;
					vect[9] = 1;
					vect[10] = 0;
					vect[11] = 1;
					break;

				case 6:
					vect[0] = 0;
					vect[1] = 1;
					vect[2] = 1;
					vect[3] = 0;
					vect[4] = 1;
					vect[5] = 0;
					vect[6] = 1;
					vect[7] = 1;
					vect[8] = 0;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 1;
					break;

				case 7:
					vect[0] = 1;
					vect[1] = 0;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 1;
					vect[5] = 0;
					vect[6] = 1;
					vect[7] = 1;
					vect[8] = 0;
					vect[9] = 1;
					vect[10] = 0;
					vect[11] = 1;
					break;

				case 8:
					vect[0] = 1;
					vect[1] = 0;
					vect[2] = 1;
					vect[3] = 0;
					vect[4] = 1;
					vect[5] = 1;
					vect[6] = 0;
					vect[7] = 0;
					vect[8] = 1;
					vect[9] = 1;
					vect[10] = 0;
					vect[11] = 1;
					break;

				case 9:
					vect[0] = 0;
					vect[1] = 1;
					vect[2] = 1;
					vect[3] = 0;
					vect[4] = 1;
					vect[5] = 1;
					vect[6] = 0;
					vect[7] = 1;
					vect[8] = 0;
					vect[9] = 1;
					vect[10] = 1;
					vect[11] = 0;
					break;

				case 10:
					vect[0] = 1;
					vect[1] = 0;
					vect[2] = 1;
					vect[3] = 1;
					vect[4] = 0;
					vect[5] = 0;
					vect[6] = 1;
					vect[7] = 1;
					vect[8] = 0;
					vect[9] = 1;
					vect[10] = 1;
					vect[11] = 0;
					break;

				case 11:
					vect[0] = 1;
					vect[1] = 0;
					vect[2] = 1;
					vect[3] = 1;
					vect[4] = 0;
					vect[5] = 1;
					vect[6] = 0;
					vect[7] = 1;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 0;
					vect[11] = 1;
					break;

				case 12:
					vect[0] = 1;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 0;
					vect[4] = 1;
					vect[5] = 1;
					vect[6] = 0;
					vect[7] = 1;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 0;
					break;

				case 13:
					vect[0] = 1;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 0;
					vect[5] = 1;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 0;
					vect[9] = 1;
					vect[10] = 1;
					vect[11] = 0;
					break;

				case 14:
					vect[0] = 0;
					vect[1] = 0;
					vect[2] = 1;
					vect[3] = 1;
					vect[4] = 0;
					vect[5] = 1;
					vect[6] = 1;
					vect[7] = 0;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 1;
					break;

				case 15:
					vect[0] = 0;
					vect[1] = 1;
					vect[2] = 0;
					vect[3] = 1;
					vect[4] = 1;
					vect[5] = 0;
					vect[6] = 0;
					vect[7] = 1;
					vect[8] = 1;
					vect[9] = 0;
					vect[10] = 1;
					vect[11] = 1;
					break;
			}
		}

		for (i=0;i<72;i++)
		{
			vect[i] = vect[i - ((int)(i/12)*12)];
		}
	}


	void fct_vect_accord()
	{
		int i,add=0,k = 0,inc = 0,retire;
		
		somme = 0;

		do
		{
			if(vect[k] == 1)
			{
				inc++;
			}
			k++;
		}
		while(inc != degre && k<72);

		switch(num_accord)
		{
			case 1:
				for(i=0;i<12;i++)
				{
					if (tonale-1+k-1+i < 12)
					{
						vect_accord[tonale-1+k-1+i] = 0;		
						
						if(vect[tonale-1+k-1+i] == 1)
						{
							add = add+1;
						}
						if ((add==1||add==3||add==5) && (vect[tonale-1+k-1+i] == 1))
						{
							vect_accord[tonale-1+k-1+i] = 1;
						}
						
					}
					else 
					{
						retire = (tonale-1+k-1+i)/12;
						vect_accord[tonale-1+k-1+i-(retire*12)] = 0;		
						
						if((tonale-1+k-1+i)<72 && vect[tonale-1+k-1+i] == 1)
						{
							add = add+1;
						}
						if ((tonale-1+k-1+i)<72 && (add==1||add==3||add== 5) && (vect[tonale-1+k-1+i] == 1))
						{
							vect_accord[tonale-1+k-1+i-(retire*12)] = 1;
						}
					}
				}
				break;

			case 2:
				for(i=0;i<12;i++)
				{
					if (tonale-1+k-1+i < 12)
					{
						vect_accord[tonale-1+k-1+i] = 0;		
						
						if(vect[tonale-1+k-1+i] == 1)
						{
							add = add+1;
						}
						if ((add==1||add==3||add==5||add==6) && (vect[tonale-1+k-1+i] == 1))
						{
							vect_accord[tonale-1+k-1+i] = 1;
						}
						
					}
					else 
					{
						retire = (tonale-1+k-1+i)/12;
						vect_accord[tonale-1+k-1+i-(retire*12)] = 0;		
						
						if((tonale-1+k-1+i)<72 && vect[tonale-1+k-1+i] == 1)
						{
							add = add+1;
						}
						if ((tonale-1+k-1+i)<72 && (add==1||add==3||add==5||add==6) && (vect[tonale-1+k-1+i] == 1))
						{
							vect_accord[tonale-1+k-1+i-(retire*12)] = 1;
						}
					}
				}
				break;

			case 3:
				for(i=0;i<12;i++)
				{
					if (tonale-1+k-1+i < 12)
					{
						vect_accord[tonale-1+k-1+i] = 0;		
						
						if(vect[tonale-1+k-1+i] == 1)
						{
							add = add+1;
						}
						if ((add==1||add==3||add==5||add==7) && (vect[tonale-1+k-1+i] == 1))
						{
							vect_accord[tonale-1+k-1+i] = 1;
						}
						
					}
					else 
					{
						retire = (tonale-1+k-1+i)/12;
						vect_accord[tonale-1+k-1+i-(retire*12)] = 0;		
						
						if((tonale-1+k-1+i)<72 && vect[tonale-1+k-1+i] == 1)
						{
							add = add+1;
						}
						if ((tonale-1+k-1+i)<72 && (add==1||add==3||add==5||add==7) && (vect[tonale-1+k-1+i] == 1))
						{
							vect_accord[tonale-1+k-1+i-(retire*12)] = 1;
						}
					}
				}
				break;

			case 4:
				for(i=0;i<12;i++)
				{
					if (tonale-1+k-1+i < 12)
					{
						vect_accord[tonale-1+k-1+i] = 0;		
						
						if(vect[tonale-1+k-1+i] == 1)
						{
							add = add+1;
						}
						if ((add==1||add==3||add==5||add==7||add==9) && (vect[tonale-1+k-1+i] == 1))
						{
							vect_accord[tonale-1+k-1+i] = 1;
						}
						
					}
					else 
					{
						retire = (tonale-1+k-1+i)/12;
						vect_accord[tonale-1+k-1+i-(retire*12)] = 0;		
						
						if((tonale-1+k-1+i)<72 && vect[tonale-1+k-1+i] == 1)
						{
							add = add+1;
						}
						if ((tonale-1+k-1+i)<72 && (add==1||add==3||add==5||add==7||add==9) && (vect[tonale-1+k-1+i] == 1))
						{
							vect_accord[tonale-1+k-1+i-(retire*12)] = 1;
						}
					}
				}
				break;
		}
		
		for(i=0;i<12;i++)
		{
			
			if(vect_accord[i] == 1)
				
				somme++;
		}
	}

}
