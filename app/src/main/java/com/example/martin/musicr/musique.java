package musicRandom;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class musique {
	
	int tonalite;
	int rythme;
	int duree_musique;
	int maj_min;
	int nbre_motifs;
	int duree_accords;
	int nbre_sol;
	int nbre_basse;
	int nbre_bat;
	int nbre_accords;
	int nbre_inst;
	
	instrument inst;
	
	int [][] voies;
	int [][] accords;
	int[][] det_inst;
	int [] choix_inst;
	int [] choix_inst_tampon;
	float [][][] partition;
	float [][][] partition_tampon;
	int [] note;
	int [] a;
	int [] a_tampon;
	int [] equilibre;
	int [] vect_accord;
	int [] vect;
		
	musique(int Mtonalite, int Mrythme, int Mduree_musique,int Mmaj_min, int Mnbre_motifs,int Mduree_accords,int Mnbre_sol,int Mnbre_basse,int Mnbre_bat){
		tonalite =  Mtonalite;
		rythme =  Mrythme;
		duree_musique = Mduree_musique;
		maj_min = Mmaj_min;
		nbre_motifs = Mnbre_motifs;
		duree_accords = Mduree_accords;
		nbre_sol = Mnbre_sol;
		nbre_basse = Mnbre_basse;
		nbre_bat = Mnbre_bat;
		
		if (tonalite == 0)
		{
			choix_tonalite();
		}
		if (rythme == 0)
		{
			choix_rythme();
		}
		if (maj_min == 0)
		{
			choix_maj_min();
		}
		if (nbre_motifs == 0)
		{
			choix_nbre_motifs();
		}
		if (duree_accords == 0)
		{
			choix_duree_accords();
		}
		if (duree_musique == 0)
		{
			choix_duree();
		}
		if (nbre_sol == -1)
		{
			choix_nbre_sol();
		}
		if (nbre_basse == -1)
		{
			choix_nbre_basse();
		}
		if (nbre_bat == -1)
		{
			choix_nbre_bat();
		}

		remplissage_det_inst();
	}
	
	// entre 1 et 15 inclus
	void choix_tonalite()
	{
	    tonalite = (int)( Math.random()* 15 ) + 1;
	    
	}

	// choix du nombre de motifs dans la musique, entre 2 et 3 inclus
	void choix_nbre_motifs()
	{
	    nbre_motifs = (int)( Math.random()*3 ) + 2;
	}

	// choix du rythme 
	void choix_rythme()
	{
	    rythme = (int)( Math.random()*161 ) + 40;
	}

	// choix de la dur�e
	void choix_duree()
	{
	    duree_musique = (int)( Math.random()*171 ) + 100;
	}

	// choix de la tonalit� majeure ou mineure
	void choix_maj_min()
	{
	    maj_min = (int)( Math.random()*2 ) + 1;
	}

	//choix du nombre d'instruments principaux
	void choix_nbre_sol()
	{
	    nbre_sol = (int)( Math.random()*3 ) + 1;
	}

	// choix du nombre d'instruments secondaires
	void choix_nbre_basse()
	{
	    nbre_basse = (int)( Math.random()*4 );
	}

	//choix du nombre de batteries
	void choix_nbre_bat()
	{
	    nbre_bat = (int)( Math.random()*2 );
	}

	//choix de la dur�e des accords, entre 2 et 8 temps inclus,cela peut correspondre a la dur�e d'une ou 2 mesures
	void choix_duree_accords()
	{
	    do 
		{
			duree_accords = (int)( Math.random()*7 ) + 2;
		}
		while(duree_accords==7||duree_accords==5);
	}
	
	// fonction principale pour la creation de la musique
	void creation_musique()
	{
		int i,j,decalage = 0,max,compt,compt_top = 0,k,p,h;

		nbre_inst = nbre_sol+nbre_basse+nbre_bat; 
		a = new int[nbre_inst];              // "a" est un tableau qui sert de curseur pour connaitre a chaque instant quelle est notre avanc� dans la musique pour chaque instrument
		a_tampon = new int[nbre_inst];
		equilibre = new int[128];           // tableau pour choisir au mieux les instruments et calculer au mieux la repartition entre les basses, aigus et medium
		note = new int[10];           // creation d'un tableau representant une note a un temps t
		choix_inst = new int [nbre_inst];    // tableau des instruments choisis
		choix_inst_tampon = new int [nbre_inst];

		nbre_accords = duree_musique/duree_accords;
		if(nbre_accords<20)
		{
			nbre_accords = 20;
			duree_musique = duree_accords*nbre_accords;
		}

		System.out.println("tonalite = "+ tonalite);
		if(maj_min == 1)
			System.out.println(" --> tonalité majeure ");
		if(maj_min == 2)
			System.out.println(" --> tonalité mineure ");
		System.out.println("nbre_motifs = " +nbre_motifs);
		System.out.println("rythme = " + rythme);
		System.out.println("duree_accords = " + duree_accords);
		System.out.println("duree musique = " + duree_musique);
		System.out.println("nombre de solistes = " + nbre_sol);
		System.out.println("nombre de basses = " + nbre_basse);
		System.out.println("nombre de batteries = " + nbre_bat);
		System.out.println("nombre d'instruments = " + nbre_inst);

		/* creartion d'un objet motif*/
		/* cet objet permet de creer la base de la musique, a savoir, la suite des accords*/
		motif moti = new motif(nbre_accords,nbre_motifs,this);
		moti.suite_accords();

		nbre_accords = moti.get_nbre_accords();
		accords = new int [5][nbre_accords];
		accords = moti.get_accords();

		voies = new int [nbre_inst][nbre_accords];
		creation_vect_voies();
		
		
		duree_musique = nbre_accords*duree_accords;
		partition = new float [nbre_inst][10][16*(duree_musique +1)];
		partition_tampon = new float [nbre_inst][10][16*(duree_musique +1)];

		for(i=0;i<nbre_sol;i++)
		{	
			choix_inst[i] = (int)( Math.random()*128 );

			System.out.println("intrument n�"+i+" = "+choix_inst[i]);

			inst = new instrument(i,i-decalage,choix_inst[i],det_inst[choix_inst[i]][0],det_inst[choix_inst[i]][1],det_inst[choix_inst[i]][3],det_inst[choix_inst[i]][2],tonalite,rythme,0,nbre_accords,this,moti);
			inst.creation_partie(maj_min);

			if(choix_inst[i] < 29)
			{
				nbre_sol++;
				i++;
				decalage++;
				
				System.arraycopy( choix_inst, 0, choix_inst_tampon, 0, choix_inst.length );
				choix_inst = new int [nbre_basse+nbre_sol+nbre_bat];
				System.arraycopy( choix_inst_tampon, 0, choix_inst, 0, choix_inst_tampon.length );
				choix_inst_tampon = new int [nbre_basse+nbre_sol+nbre_bat];
				
				System.arraycopy( a, 0, a_tampon, 0, a.length );
				a = new int [nbre_basse+nbre_sol+nbre_bat];
				System.arraycopy( a_tampon, 0, a, 0, a_tampon.length );
				a_tampon = new int [nbre_basse+nbre_sol+nbre_bat];
				
				System.arraycopy( partition, 0, partition_tampon, 0, partition.length );
				partition = new float [nbre_basse+nbre_sol+nbre_bat][10][16*duree_musique +1];
				System.arraycopy( partition_tampon, 0, partition, 0, partition_tampon.length );
				partition_tampon = new float [nbre_basse+nbre_sol+nbre_bat][10][16*duree_musique +1];
				
				inst = new instrument(i,i-decalage,128 + choix_inst[i],det_inst[128 + choix_inst[i]][0],det_inst[128 + choix_inst[i]][1],det_inst[128 + choix_inst[i]][3],det_inst[128 + choix_inst[i]][2],tonalite,rythme,0,nbre_accords,this,moti);
				inst.creation_partie(maj_min);
			}
		}
		for(j=nbre_sol;j<nbre_basse+nbre_sol;j++)
		{	
			choix_inst[j] = (int)( Math.random()*128 );

			System.out.println("intrument n�"+j+" = "+choix_inst[j]);
			inst = new instrument(j,j-decalage,choix_inst[j],det_inst[choix_inst[j]][0],det_inst[choix_inst[j]][1],det_inst[choix_inst[j]][3],det_inst[choix_inst[j]][2],tonalite,rythme,1,nbre_accords,this,moti);
			inst.creation_partie(maj_min);

			if(choix_inst[j] < 29)
			{
				nbre_basse++;
				j++;
				decalage++;
				
				System.arraycopy( choix_inst, 0, choix_inst_tampon, 0, choix_inst.length );
				choix_inst = new int [nbre_basse+nbre_sol+nbre_bat];
				System.arraycopy( choix_inst_tampon, 0, choix_inst, 0, choix_inst_tampon.length );
				choix_inst_tampon = new int [nbre_basse+nbre_sol+nbre_bat];
				
				System.arraycopy( a, 0, a_tampon, 0, a.length );
				a = new int [nbre_basse+nbre_sol+nbre_bat];
				System.arraycopy( a_tampon, 0, a, 0, a_tampon.length );
				a_tampon = new int [nbre_basse+nbre_sol+nbre_bat];
				
				System.arraycopy( partition, 0, partition_tampon, 0, partition.length );
				partition = new float [nbre_basse+nbre_sol+nbre_bat][10][16*duree_musique +1];
				System.arraycopy( partition_tampon, 0, partition, 0, partition_tampon.length );
				partition_tampon = new float [nbre_basse+nbre_sol+nbre_bat][10][16*duree_musique +1];
				
				inst = new instrument(j,j-decalage,128 + choix_inst[j],det_inst[128 + choix_inst[j]][0],det_inst[128+ choix_inst[j]][1],det_inst[128+ choix_inst[j]][3],det_inst[128+ choix_inst[j]][2],tonalite,rythme,1,nbre_accords,this,moti);
				inst.creation_partie(maj_min);
			}
		}
		for(j=nbre_basse+nbre_sol;j<nbre_basse+nbre_sol+nbre_bat;j++)
		{
			choix_inst[j] = 200;
			System.out.println("intrument n�"+j+" = "+choix_inst[j]);
			inst = new instrument(j,j-decalage,choix_inst[j],600,48,35,81,tonalite,rythme,1,nbre_accords,this,moti);
			inst.creation_partie(maj_min);
		}

		nbre_inst = nbre_sol+nbre_basse+nbre_bat;

		try {
			write_midi();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		try {
			write_test();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	public byte[] longToBytes(long x) {
	    ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
	    buffer.putLong(x);
	    
	    return buffer.array();
	}
	
	public byte[] fourToBytes(int x) {
	    ByteBuffer buffer = ByteBuffer.allocate(4);
	    buffer.putInt(x);
	    return buffer.array();
	}
	
	public byte[] threeToBytes(int x) {
	    ByteBuffer buffer = ByteBuffer.allocate(4);
	    ByteBuffer buffer2 = ByteBuffer.allocate(3);
	    buffer.putInt(x);
	    buffer2.array()[0] = buffer.array()[1];
	    buffer2.array()[1] = buffer.array()[2];
	    buffer2.array()[2] = buffer.array()[3];
	    return buffer2.array();
	}
	
	public byte[] shortToBytes(short x) {
	    ByteBuffer buffer = ByteBuffer.allocate(Short.BYTES);
	    buffer.putShort(x);
	    return buffer.array();
	}
	
	void write_test() throws IOException
	{
		FileOutputStream file_midi = new FileOutputStream ("essai2.mid");
		
		int t_note_midi;
		 char velocity;
		 char num_note_midi;
		 int length = 6;
		 int length_bis = 25;
		 int [] length_part = new int [16];// = piano.get_nbre_note()*14 + 4;
		 int length_part_3;
		 short formatType = 1;
		 short nbreVoies = 1; //nbre_inst+2;
		 short PPQN = 160;//384; 
		 int tempo = (int)((1000000.0 * 60.0)/((float)rythme)); //microseconds/noire 

		 char ff = 0xff;
		 char a_51 = 0x51;
		 char [] num_inst = new char[16];
		 char a_03 = 0x03;
		 char a_20 = 0x20;
		 char a_01 = 0x01;
		 char a_58 = 0x58;
		 char a_04 = 0x04;
		 char a_59 = 0x59;
		 char a_02 = 0x02;
		 char [] n_on = new char[16];
		 char [] n_off = new char[16];
		 char a_2f = 0x2f;
		 char [] c_ = new char[16];
		 char zero = 0x00;
		 char num;
		 char den = 2; // 2 -> 4, 3 ->8 ...etc
		 char cc = 24;
		 char bb = 8;
		 char sf;
		 char mi;
		 char channel;
		 char temp;

		char non = 0x90;
		n_on[0] = 0x91;
		n_on[1] = 0x92;
		n_on[2] = 0x93;
		n_on[3] = 0x94;
		n_on[4] = 0x95;
		n_on[5] = 0x96;
		n_on[6] = 0x97;
		n_on[7] = 0x98;
		n_on[9] = 0x99;

		char noff = 0x80;
		n_off[0] = 0x81;
		n_off[1] = 0x82;
		n_off[2] = 0x83;
		n_off[3] = 0x84;
		n_off[4] = 0x85;
		n_off[5] = 0x86;
		n_off[6] = 0x87;
		n_off[7] = 0x88;
		n_off[9] = 0x89;
		
		char c = 0xc0;
		c_[0] = 0xc1;
		c_[1] = 0xc2;
		c_[2] = 0xc3;
		c_[3] = 0xc4;
		c_[4] = 0xc5;
		c_[5] = 0xc6;
		c_[6] = 0xc7;
		c_[7] = 0xc8;
		c_[9] = 0xc9;
	
		long buffer;
		
		file_midi.write("MThd".getBytes());
		file_midi.write(fourToBytes(length));
		file_midi.write(shortToBytes(formatType));
		file_midi.write(shortToBytes(nbreVoies));
		file_midi.write(shortToBytes(PPQN));

		file_midi.write("MTrk".getBytes());
		file_midi.write(fourToBytes(34));
		
		temp = (char)choix_inst[0];

		file_midi.write(zero);
		file_midi.write(c);
		file_midi.write(zero);
		
		num_note_midi = 50;
		velocity = 10;
		
		file_midi.write(zero);
		file_midi.write(non);
		file_midi.write(num_note_midi); //numéro de la note
		file_midi.write(velocity); //vélocité de la note
		
		num_note_midi = 60;
		
		file_midi.write(zero);
		file_midi.write(non);
		file_midi.write(num_note_midi); //numéro de la note
		file_midi.write(velocity); //vélocité de la note
		
		num_note_midi = 70;
		
		file_midi.write(zero);
		file_midi.write(non);
		file_midi.write(num_note_midi); //numéro de la note
		file_midi.write(velocity); //vélocité de la note
		
		num_note_midi = 50;
		t_note_midi = 160;
		
		writeVariableLengthInt(160,file_midi);
		//file_midi.write(WriteVarLen(t_note_midi));
		file_midi.write(noff);
		file_midi.write(num_note_midi); //numéro de la note
		file_midi.write(velocity); //vélocité de la note
		
		num_note_midi = 60;
		
		//file_midi.write(WriteVarLen(t_note_midi));
		writeVariableLengthInt(160,file_midi);
		file_midi.write(noff);
		file_midi.write(num_note_midi); //numéro de la note
		file_midi.write(velocity); //vélocité de la note
		
		num_note_midi = 70;
		
		//file_midi.write(WriteVarLen(t_note_midi));
		writeVariableLengthInt(160,file_midi);
		file_midi.write(noff);
		file_midi.write(num_note_midi); //numéro de la note
		file_midi.write(velocity); //vélocité de la note
		
		file_midi.write(zero);
		file_midi.write(ff);
		file_midi.write(a_2f);
		file_midi.write(zero);
		
		file_midi.close();
	}
	
/*	int sizeWriteVarLen(long value) 
	{
		byte[] bytes = ByteBuffer.allocate(Long.SIZE / Byte.SIZE).putLong(value).array();
		
		int size = bytes.length;
		int i = 0;
		
		while (bytes[i] == 0 && i <(size-1))
	    {
	    	i++;
	    }
		
		return (size - i);
	}*/
	
	public synchronized void writeVariableLengthInt (int value, FileOutputStream file_midi) throws IOException
	{
    int buffer = value & 0x7F;

    while ((value >>= 7) != 0)
    {
		buffer <<= 8;
		buffer |= ((value & 0x7F) | 0x80);
    }
      
    while (true)
      {
    	file_midi.write(buffer & 0xff);
		if ((buffer & 0x80) != 0)
		  buffer >>>= 8;
		else
		  break;
      }
	}
	
	public int variableLengthIntLength (int value)
	{
    int length = 0;
    int buffer = value & 0x7F;

    while ((value >>= 7) != 0)
    {
		buffer <<= 8;
		buffer |= ((value & 0x7F) | 0x80);
    }
      
    while (true)
    {
	length++;
	if ((buffer & 0x80) != 0)
		buffer >>>= 8;
	else
		break;
    }

    return length;
	}
	
	/*byte[] WriteVarLen(long value)
	{
		byte[] bytes = ByteBuffer.allocate(Long.SIZE / Byte.SIZE).putLong(value).array();
		
		int size = bytes.length;
		int u,i = 0,size_new_bytes;
		
		while (bytes[i] == 0 && i <(size-1))
	    {
	    	i++;
	    }
		
		size_new_bytes = size - i;
	    
	    byte[] temp = new byte[size_new_bytes];
	    
	    for (u=0;u<size_new_bytes;u++) 
	    {
	    	temp[u] = bytes[u+i];
	    }

	    return temp;
	}*/
	
	void write_midi() throws IOException
	{
		int oct = 0,no,u,i,q,compt=0,z=1,compt_1=0,k,inc,tonale, dec = 0,sil,temporaire,x;
		short t_note = 1;

		vect = new int [72];
		vect_accord = new int [12];

		FileOutputStream file_midi = new FileOutputStream ("essai1.mid");

		if (oct == oct)
		{
			 int t_note_midi;
			 char velocity;
			 char num_note_midi;
			 int length = 6;
			 int length_bis = 25;
			 int [] length_part = new int [16];// = piano.get_nbre_note()*14 + 4;
			 int length_part_3;
			 short formatType = 1;
			 short nbreVoies = (short)(nbre_inst+1); //nbre_inst+2;
			 short PPQN = 160;//384; 
			 int tempo = (int)((1000000.0 * 60.0)/((float)rythme)); //microseconds/noire 

			 char ff = 0xff;
			 char a_51 = 0x51;
			 char [] num_inst = new char[16];
			 char a_03 = 0x03;
			 char a_20 = 0x20;
			 char a_01 = 0x01;
			 char a_58 = 0x58;
			 char a_04 = 0x04;
			 char a_59 = 0x59;
			 char a_02 = 0x02;
			 char [] n_on = new char[16];
			 char [] n_off = new char[16];
			 char a_2f = 0x2f;
			 char [] c_ = new char[16];
			 char zero = 0x00;
			 char num;
			 char den = 2; // 2 -> 4, 3 ->8 ...etc
			 char cc = 24;
			 char bb = 8;
			 char sf;
			 char mi;
			 char channel;
			 char temp;

			n_on[0] = 0x91;
			n_on[1] = 0x92;
			n_on[2] = 0x93;
			n_on[3] = 0x94;
			n_on[4] = 0x95;
			n_on[5] = 0x96;
			n_on[6] = 0x97;
			n_on[7] = 0x98;
			n_on[9] = 0x99;

			n_off[0] = 0x81;
			n_off[1] = 0x82;
			n_off[2] = 0x83;
			n_off[3] = 0x84;
			n_off[4] = 0x85;
			n_off[5] = 0x86;
			n_off[6] = 0x87;
			n_off[7] = 0x88;
			n_off[9] = 0x89;
			
			c_[0] = 0xc1;
			c_[1] = 0xc2;
			c_[2] = 0xc3;
			c_[3] = 0xc4;
			c_[4] = 0xc5;
			c_[5] = 0xc6;
			c_[6] = 0xc7;
			c_[7] = 0xc8;
			c_[9] = 0xc9;
		
			long buffer;
/*
			length = convert_endian_32(length);
			length_bis = convert_endian_32(length_bis);
			formatType = convert_endian_16(formatType);
			nbreVoies = convert_endian_16(nbreVoies);
			PPQN = convert_endian_16(PPQN);
			tempo = convert_endian_24(tempo);
*/
			// write midi MThd chunk
			file_midi.write("MThd".getBytes());
			file_midi.write(fourToBytes(length));
			file_midi.write(shortToBytes(formatType));
			file_midi.write(shortToBytes(nbreVoies));
			file_midi.write(shortToBytes(PPQN));

			// write midi MTrk chunks et options
			file_midi.write("MTrk".getBytes());
			file_midi.write(fourToBytes(length_bis));
			//tempo
			file_midi.write(zero);
			file_midi.write(ff);
			file_midi.write(a_51);
			file_midi.write(a_03);
			file_midi.write(threeToBytes(tempo));

			//time signature 4/4 2/4 ...etc
			if((duree_accords == 3) || (duree_accords ==6))
				num = 3;
			else
				num = 4;
			
			file_midi.write(zero);
			file_midi.write(ff);
			file_midi.write(a_58);
			file_midi.write(a_04);
			file_midi.write(num);
			file_midi.write(den);
			file_midi.write(cc);
			file_midi.write(bb);

			//key signature
			sf = (char)(tonalite - 8);
			mi = (char)(maj_min - 1);
			file_midi.write(zero);
			file_midi.write(ff);
			file_midi.write(a_59);
			file_midi.write(a_02);
			file_midi.write(sf);
			file_midi.write(mi);

			file_midi.write(zero);
			file_midi.write(ff);
			file_midi.write(a_2f);
			file_midi.write(zero);
			
			boolean yt = true;

			for(i=0;i<nbre_inst;i++)
			{
				dec = 0;
				compt = 0;
				temp = (char)choix_inst[i];

				//compteur duree voie_1
				for (u=0;u<duree_musique*16;u++)
				{
					oct = (int)(partition[i][1][u]);
					no = (int)(partition[i][2][u]);
					sil = (int)(partition[i][0][u]);

					if ((oct == partition[i][1][u+1]) && (no == partition[i][2][u+1]) && (partition[i][3][u] == 0))
					{	
						t_note++;
					}
					else
					{
						if(sil != 0)
						{
							t_note_midi = (short)dec;
							
							compt = compt + variableLengthIntLength(t_note_midi) + 3;

							for(x=5;x<8;x++)
							{
								if(partition[i][x][u] != 0)
								{
									compt = compt + 4;
								}
							}

							t_note_midi = (short)(t_note*10); //*PPQN/16.0;
							
							compt = compt + variableLengthIntLength(t_note_midi) + 3;

							for(x=5;x<8;x++)
							{
								if(partition[i][x][u] != 0)
								{
									compt = compt + 4;
								}
							}
							
							t_note = 1;
							dec = 0;
							
						}
						else
						{
							dec = dec + t_note*10;
							t_note = 1;
						}
							
					}
				}
				dec = 0;

				length_part[i] = compt + 12;
				channel = (char)(i+1);

				if((i == nbre_inst-1) && (nbre_bat == 1))
				{
					channel = 10;
				} 

				file_midi.write("MTrk".getBytes());
				file_midi.write(fourToBytes(length_part[i]));
				file_midi.write(zero);
				file_midi.write(ff);
				file_midi.write(a_20);
				file_midi.write(a_01);
				file_midi.write(channel);

				file_midi.write(zero);
				file_midi.write(c_[i]);
				file_midi.write(temp);

				for (u=0;u<duree_musique*16;u++)
				{
					oct = (int)(partition[i][1][u]);
					no = (int)(partition[i][2][u]);
					sil = (int)(partition[i][0][u]);
					velocity = (char)partition[i][4][u];
		
					if ((oct == partition[i][1][u+1]) && (no == partition[i][2][u+1]) && (partition[i][3][u] == 0))
					{	
						t_note++;
					}
					else
					{
						if(sil != 0)
						{
							t_note_midi = (short)dec;

							num_note_midi = (char)(oct*12 + no + 23);

							// note on
							//file_midi.write(WriteVarLen(t_note_midi));
							writeVariableLengthInt(t_note_midi,file_midi);
							
							if((i == nbre_inst-1) && (nbre_bat == 1))
							{
								file_midi.write(n_on[9]); // note on 9(channel 10)
							}
							else
							{
								file_midi.write(n_on[i]);
							}
							file_midi.write(num_note_midi); //numéro de la note
							file_midi.write(velocity); //vélocité de la note

							for(x=5;x<8;x++)
							{
								if(partition[i][x][u] != 0)
								{	
									t_note_midi = 0;

									num_note_midi = (char)(partition[i][x][u] + 23);

									// note on
									//file_midi.write(WriteVarLen(t_note_midi));
									writeVariableLengthInt(t_note_midi,file_midi);
									
									if((i == nbre_inst-1) && (nbre_bat == 1))
									{
										file_midi.write(n_on[9]); // note on 9(channel 10)
									}
									else
									{
										file_midi.write(n_on[i]);
									}
									file_midi.write(num_note_midi); //numéro de la note
									file_midi.write(velocity); //vélocité de la note
								}
							}

							velocity = 0;
							t_note_midi = (short)(t_note*10); //*PPQN/16.0;
							num_note_midi = (char)(oct*12 + no + 23);

							// note off
							//file_midi.write(WriteVarLen(t_note_midi));
							writeVariableLengthInt(t_note_midi,file_midi);
							
							if((i == nbre_inst-1) && (nbre_bat == 1))
							{
								file_midi.write(n_off[9]); // note on 9(channel 10)
							}
							else
							{
								file_midi.write(n_off[i]);
							}
							file_midi.write(num_note_midi); //numéro de la note
							file_midi.write(velocity); //vélocité de la note

							for(x=5;x<8;x++)
							{
								if(partition[i][x][u] != 0)
								{	
									t_note_midi = 0;

									num_note_midi = (char)(partition[i][x][u] + 23);

									// note on
									//file_midi.write(WriteVarLen(t_note_midi));
									writeVariableLengthInt(t_note_midi,file_midi);
									
									if((i == nbre_inst-1) && (nbre_bat == 1))
									{
										file_midi.write(n_off[9]); // note on 9(channel 10)
									}
									else
									{
										file_midi.write(n_off[i]);
									}
									file_midi.write(num_note_midi); //numéro de la note
									file_midi.write(velocity); //vélocité de la note
								}
							}

							t_note = 1;
							dec = 0;
						}
						else
						{
							dec = dec + t_note*10;
							t_note = 1;
						}
					}

				}
				file_midi.write(zero);
				file_midi.write(ff); 
				file_midi.write(a_2f);
				file_midi.write(zero); 
			}
			System.out.println("Saved midi\n");
			
			file_midi.close();
		}
		else
			System.out.println("Could not open to write midi data\n");	
	}
	
	int valeur_vect_voies(int i,int j)
	{
		return voies[i][j];
	}

	void creation_vect_voies()
	{
		int i,j,k=0,somme = 0,decalage,dec = 0,g,d,al,test,p,deb_motif,w,nbre_1,nbre_2,nbre_3,nbre_4;
		
		int [][] decre = new int [nbre_motifs][2*nbre_sol];

		for(i=0;i<nbre_motifs;i++)
		{
			for(j=0;j<nbre_sol;j++)
			{
				if(j==0)
				{	
					decre[i][j] = (int)( Math.random()*26 ) + 75;
					decre[i][j+nbre_sol] = (int)( Math.random()*16 );
				}
				else
				{
					decre[i][j] = (int)( Math.random()*81 ) + 20;
					decre[i][j+nbre_sol] = (int)( Math.random()*51 ) + 10;
				}
			}
		}

		for(i=nbre_sol;i<(nbre_sol+nbre_basse);i++)
		{
			for(j=0;j<nbre_accords;j++)
			{
				voies[i][j] = 2;
				if(j == 0)
					voies[i][j] = 3;
			}
		}

		if(nbre_basse == 0 && accords[2][0] == 0)
		{
			k = 0;
			do
			{
				nbre_3 = (int)( Math.random()*nbre_sol ) + 1;
							
				for(j=0;j<nbre_3;j++)
				{
					nbre_4 = (int)( Math.random()*nbre_sol ) ;
					voies[nbre_4][k] = 2;
				}
				k++;
			}
			while(accords[2][k] == 0);


			for(i=0;i<nbre_sol;i++)
			{
				for(j=0;j<k;j++)
				{
					deb_motif = 0;
					if(voies[i][j] == 2 && deb_motif==0)
					{
						voies[i][j] = 3;
						deb_motif = 1;
					}
				}
			}
		}

		for(i=0;i<nbre_motifs;i++)
		{
			k=0;
			test = 0;

			do 
			{
				if((accords[2][k] == (i+1)) && (accords[4][k]>0))
				{
					somme = accords[4][k] + accords[4][k+accords[4][k]];
				
					if(test == 0)
					{
						dec = (int)( Math.random()*nbre_sol ) ;
					}

					for(j=dec;j<(nbre_sol+dec);j++)
					{
						d = 1000;
						g = j;
						if(g>=nbre_sol)
						{
							g = g-nbre_sol;
						}

						nbre_1 = (int)((decre[i][g]/100.0)*somme);
						nbre_2 = (int)((decre[i][g+nbre_sol]/100.0)*somme);

						decalage = (int)( Math.random()*(somme-nbre_1 + 1) );

						for(p=decalage;p<(nbre_1+decalage);p++)
						{
							voies[j-dec][k+p] = 1;
						}

						if(nbre_2 > 0)
						{
							do
							{
								al = (int)( Math.random()*somme );
								if(voies[j-dec][k+al] == 0)
								{
									voies[j-dec][k+al] = 2;
									nbre_2 = nbre_2 - 1;
								}
								d--;
							}
							while(nbre_2>0 && d>0);
						}
					}

					if(nbre_basse == 0)
					{
						for(w=k;w<k+somme;w++)
						{
							test = 0;
							for(j=0;j<nbre_sol;j++)
							{		
								if(voies[j][w] != 0 )
								{
									test = 1;
								}
							}
							if(test == 0)
							{
								nbre_3 = (int)( Math.random()*nbre_sol );
							
								for(j=0;j<nbre_3;j++)
								{
									nbre_4 = (int)( Math.random()*nbre_sol );
									voies[nbre_4][w] = 2;
								}
							}
							
						}
					}

					for(j=0;j<nbre_sol;j++)
					{
						deb_motif = 0;
						for(w=k;w<k+somme;w++)
						{		
							if(voies[j][w] == 2 && deb_motif==0)
							{
								voies[j][w] = 3;
								deb_motif = 1;
							}
						}
					}
				
					test = 1;
					k = k + somme;
				}
				else
				{
					k++;
				}
			}
			while(k<nbre_accords);
		}	

		if(nbre_basse > 0)
		{
			k = 1;
			do
			{
				test = 0;
				for(j=0;j<nbre_sol;j++)
				{		
					if(voies[j][k] != 0 )
					{
						test = 1;
					}
				}
				if(test == 0)
				{
					nbre_3 = (int)( Math.random()*(nbre_basse+1) );
				
					for(j=0;j<nbre_3;j++)
					{
						nbre_4 = (int)( Math.random()*nbre_sol );
						voies[nbre_4][k] = 1;
					}
				}
				k++;
			}
			while(k<nbre_accords);
		}

		for(i=(nbre_sol+nbre_basse);i<nbre_inst;i++)
		{
			for(j=0;j<nbre_accords;j++)
			{
				voies[i][j] = 4;
				if(j == 0)
					voies[i][j] = 3;
			}
		}
	}
	
	int[] get_note(int temps, int num_instrument)
	{	
		int i;

		for(i=0;i<10;i++)
		{
			note[i] = (int)partition[num_instrument][i][temps];
		}

		return note;
	}

	int get_duree_note(int temps, int num_instrument)
	{	
//		cout << "function get_duree_note "<< endl;
		int duree = 0;

		do
		{
			temps--;
			duree ++;
		}
		while(partition[num_instrument][3][temps] != 1 && temps > -1);

		return duree;
	}

	void add_parti(float [][] parti, int c, int voie)
	{
//		cout << "function add_parti "<< endl;
		int i,k;

		for(i=0;i<c;i++)
		{
			for(k=0;k<10;k++)
			{
				partition[voie][k][a[voie]+i] = parti[k][i];
			}
		}
		a[voie] = a[voie] + c;
	}

	void set_partition(int oct, int no,int duree_note, int num_instrument, int sil,int volume,int note_1,int note_2,int note_3)
	{
//		cout << "function set_partition "<< endl;
		int u = 0, duree_note_1 = 0;

		if(duree_note == 3)
			duree_note_1 = 4;
		if(duree_note == 4)
			duree_note_1 = 8;
		if(duree_note == 5)
			duree_note_1 = 16;
		if(duree_note == 6)
			duree_note_1 = 24;
		if(duree_note == 7)
			duree_note_1 = 32;
		if(duree_note == 8)
			duree_note_1 = 48;
		if(duree_note == 9)
			duree_note_1 = 64;

		duree_note = duree_note_1;
		
		if(a[num_instrument]+duree_note > 16*(duree_musique +1))
		{
			System.arraycopy( partition, 0, partition_tampon, 0, partition.length );
			partition = new float [nbre_basse+nbre_sol+nbre_bat][10][a[num_instrument]+duree_note];
			System.arraycopy( partition_tampon, 0, partition, 0, partition_tampon.length );
		}

		for (u=a[num_instrument];u<a[num_instrument]+duree_note;u++)
		{
			partition[num_instrument][0][u] = sil;          //silence
			partition[num_instrument][1][u] = oct;	        //octave
			partition[num_instrument][2][u] = no;           //num note
			partition[num_instrument][3][u] = 0;            //indicateur fin duree note
			partition[num_instrument][4][u] = volume;            //volume
			partition[num_instrument][5][u] = note_1;           // note supplémentaires accords 
			partition[num_instrument][6][u] = note_2;
			partition[num_instrument][7][u] = note_3;
			partition[num_instrument][8][u] = 1;
			partition[num_instrument][9][u] = 1;            //details
		}
		partition[num_instrument][3][a[num_instrument]+duree_note-1] = 1;

		a[num_instrument] = u;
	}

	int get_a(int num_instrument)
	{
//		cout << "function get_a "<< endl;
		return a[num_instrument];
	}

	int get_duree_musique()
	{
//		cout << "function get_duree_musique "<< endl;
		return duree_musique;
	}

	int get_duree_accords()
	{
//		cout << "function get_duree_accords "<< endl;
		return duree_accords;
	}

	void remplissage_det_inst()
	{
	//	cout << "function remplissage_det_inst"<< endl;
		det_inst = new int[155][4];
	
		//piano grand acoustique voie haute
		det_inst[0][0] = 600;     // rythme max absolu
		det_inst[0][1] = 48;     //note reference
		det_inst[0][2] = 72;      // note max
		det_inst[0][3] = 30;      // note min
	
		//piano acoustique
		det_inst[1][0] = 600;     // rythme max absolu
		det_inst[1][1] = 48;     //note reference
		det_inst[1][2] = 72;      // note max
		det_inst[1][3] = 30;      // note min
	
		//piano grand electrique
		det_inst[2][0] = 600;     // rythme max absolu
		det_inst[2][1] = 47;     //note reference
		det_inst[2][2] = 64;      // note max
		det_inst[2][3] = 30;      // note min
	
		//piano bastringue
		det_inst[3][0] = 600;     // rythme max absolu
		det_inst[3][1] = 47;     //note reference
		det_inst[3][2] = 64;      // note max
		det_inst[3][3] = 30;      // note min
	
		//piano electrique
		det_inst[4][0] = 600;     // rythme max absolu
		det_inst[4][1] = 47;     //note reference
		det_inst[4][2] = 64;      // note max
		det_inst[4][3] = 30;      // note min
	
		//piano effet chorus
		det_inst[5][0] = 400;     // rythme max absolu
		det_inst[5][1] = 47;     //note reference
		det_inst[5][2] = 64;      // note max
		det_inst[5][3] = 30;      // note min
	
		//clavecin
		det_inst[6][0] = 500;     // rythme max absolu
		det_inst[6][1] = 47;     //note reference
		det_inst[6][2] = 64;      // note max
		det_inst[6][3] = 30;      // note min
	
		//clavinet
		det_inst[7][0] = 500;     // rythme max absolu
		det_inst[7][1] = 47;     //note reference
		det_inst[7][2] = 64;      // note max
		det_inst[7][3] = 30;      // note min
	
		//celesta
		det_inst[8][0] = 800;     // rythme max absolu
		det_inst[8][1] = 47;     //note reference
		det_inst[8][2] = 64;      // note max
		det_inst[8][3] = 30;      // note min
	
		//Glockenspiel
		det_inst[9][0] = 800;     // rythme max absolu
		det_inst[9][1] = 47;     //note reference
		det_inst[9][2] = 64;      // note max
		det_inst[9][3] = 30;      // note min
	
		//boite a musique
		det_inst[10][0] = 800;     // rythme max absolu
		det_inst[10][1] = 47;     //note reference
		det_inst[10][2] = 64;      // note max
		det_inst[10][3] = 30;      // note min
	
		//Vibraphone
		det_inst[11][0] = 800;     // rythme max absolu
		det_inst[11][1] = 47;     //note reference
		det_inst[11][2] = 64;      // note max
		det_inst[11][3] = 30;      // note min
	
		//marimba
		det_inst[12][0] = 800;     // rythme max absolu
		det_inst[12][1] = 47;     //note reference
		det_inst[12][2] = 64;      // note max
		det_inst[12][3] = 30;      // note min
	
		//Xylophone
		det_inst[13][0] = 800;     // rythme max absolu
		det_inst[13][1] = 47;     //note reference
		det_inst[13][2] = 64;      // note max
		det_inst[13][3] = 30;      // note min
	
		//cloches tubulaires
		det_inst[14][0] = 800;     // rythme max absolu
		det_inst[14][1] = 47;     //note reference
		det_inst[14][2] = 64;      // note max
		det_inst[14][3] = 30;      // note min
	
		//Tympanon
		det_inst[15][0] = 800;     // rythme max absolu
		det_inst[15][1] = 47;     //note reference
		det_inst[15][2] = 64;      // note max
		det_inst[15][3] = 30;      // note min
	
		//Orgue hammond
		det_inst[16][0] = 500;     // rythme max absolu
		det_inst[16][1] = 47;     //note reference
		det_inst[16][2] = 64;      // note max
		det_inst[16][3] = 30;      // note min
	
		//Orgue a percussion
		det_inst[17][0] = 500;     // rythme max absolu
		det_inst[17][1] = 47;     //note reference
		det_inst[17][2] = 64;      // note max
		det_inst[17][3] = 30;      // note min
	
		//Orgue Rock
		det_inst[18][0] = 500;     // rythme max absolu
		det_inst[18][1] = 47;     //note reference
		det_inst[18][2] = 64;      // note max
		det_inst[18][3] = 30;      // note min
	
		//grandes orgues
		det_inst[19][0] = 300;     // rythme max absolu
		det_inst[19][1] = 47;     //note reference
		det_inst[19][2] = 64;      // note max
		det_inst[19][3] = 30;      // note min
	
		//harmonium
		det_inst[20][0] = 300;     // rythme max absolu
		det_inst[20][1] = 47;     //note reference
		det_inst[20][2] = 64;      // note max
		det_inst[20][3] = 30;      // note min
	
		//accordeon
		det_inst[21][0] = 600;     // rythme max absolu
		det_inst[21][1] = 47;     //note reference
		det_inst[21][2] = 64;      // note max
		det_inst[21][3] = 30;      // note min
	
		//Harmonica
		det_inst[22][0] = 400;     // rythme max absolu
		det_inst[22][1] = 47;     //note reference
		det_inst[22][2] = 60;      // note max
		det_inst[22][3] = 36;      // note min
	
		//accordeon tango
		det_inst[23][0] = 600;     // rythme max absolu
		det_inst[23][1] = 47;     //note reference
		det_inst[23][2] = 64;      // note max
		det_inst[23][3] = 30;      // note min
	
		//guitare classique
		det_inst[24][0] = 600;     // rythme max absolu
		det_inst[24][1] = 47;     //note reference
		det_inst[24][2] = 63;      // note max
		det_inst[24][3] = 30;      // note min
	
		//guitare seche
		det_inst[25][0] = 600;     // rythme max absolu
		det_inst[25][1] = 42;     //note reference
		det_inst[25][2] = 63;      // note max
		det_inst[25][3] = 30;      // note min
	
		//guitare electrique - jazz
		det_inst[26][0] = 600;     // rythme max absolu
		det_inst[26][1] = 47;     //note reference
		det_inst[26][2] = 63;      // note max
		det_inst[26][3] = 30;      // note min
	
		//guitare electrique - son clair
		det_inst[27][0] = 600;     // rythme max absolu
		det_inst[27][1] = 47;     //note reference
		det_inst[27][2] = 63;      // note max
		det_inst[27][3] = 30;      // note min
	
		//guitare electrique - sourdine
		det_inst[28][0] = 600;     // rythme max absolu
		det_inst[28][1] = 47;     //note reference
		det_inst[28][2] = 63;      // note max
		det_inst[28][3] = 30;      // note min
	
		//guitare saturee
		det_inst[29][0] = 600;     // rythme max absolu
		det_inst[29][1] = 47;     //note reference
		det_inst[29][2] = 63;      // note max
		det_inst[29][3] = 30;      // note min
	
		//guitare distorssion
		det_inst[30][0] = 600;     // rythme max absolu
		det_inst[30][1] = 47;     //note reference
		det_inst[30][2] = 63;      // note max
		det_inst[30][3] = 30;      // note min
	
		//harmoniques guitare
		det_inst[31][0] = 600;     // rythme max absolu
		det_inst[31][1] = 47;     //note reference
		det_inst[31][2] = 63;      // note max
		det_inst[31][3] = 30;      // note min
	
		//basse acoustique sans frete 
		det_inst[32][0] = 600;     // rythme max absolu
		det_inst[32][1] = 24;     //note reference
		det_inst[32][2] = 40;      // note max
		det_inst[32][3] = 4;      // note min
	
		//basse electrique
		det_inst[33][0] = 600;     // rythme max absolu
		det_inst[33][1] = 24;     //note reference
		det_inst[33][2] = 40;      // note max
		det_inst[33][3] = 4;      // note min
	
		//basse electrique mediator 
		det_inst[34][0] = 600;     // rythme max absolu
		det_inst[34][1] = 24;     //note reference
		det_inst[34][2] = 40;      // note max
		det_inst[34][3] = 4;      // note min
	
		//basse sans frettes 
		det_inst[35][0] = 600;     // rythme max absolu
		det_inst[35][1] = 24;     //note reference
		det_inst[35][2] = 40;      // note max
		det_inst[35][3] = 4;      // note min
	
		//basse slap 1 
		det_inst[36][0] = 600;     // rythme max absolu
		det_inst[36][1] = 24;     //note reference
		det_inst[36][2] = 40;      // note max
		det_inst[36][3] = 4;      // note min
	
		//basse slap 2
		det_inst[37][0] = 600;     // rythme max absolu
		det_inst[37][1] = 24;     //note reference
		det_inst[37][2] = 40;      // note max
		det_inst[37][3] = 4;      // note min
	
		//basse synthe 1
		det_inst[38][0] = 600;     // rythme max absolu
		det_inst[38][1] = 24;     //note reference
		det_inst[38][2] = 40;      // note max
		det_inst[38][3] = 4;      // note min
	
		//basse synthe 2
		det_inst[39][0] = 600;     // rythme max absolu
		det_inst[39][1] = 24;     //note reference
		det_inst[39][2] = 40;      // note max
		det_inst[39][3] = 4;      // note min
	
		//violon
		det_inst[40][0] = 600;     // rythme max absolu
		det_inst[40][1] = 45;     //note reference
		det_inst[40][2] = 65;      // note max
		det_inst[40][3] = 31;      // note min
	
		//violon alto
		det_inst[41][0] = 600;     // rythme max absolu
		det_inst[41][1] = 45;     //note reference
		det_inst[41][2] = 65;      // note max
		det_inst[41][3] = 31;      // note min
	
		//violoncelle
		det_inst[42][0] = 600;     // rythme max absolu
		det_inst[42][1] = 24;     //note reference
		det_inst[42][2] = 40;      // note max
		det_inst[42][3] = 12;      // note min
	
		//contre basse
		det_inst[43][0] = 500;     // rythme max absolu
		det_inst[43][1] = 24;     //note reference
		det_inst[43][2] = 48;      // note max
		det_inst[43][3] = 10;      // note min
	
		//cordes tremolo
		det_inst[44][0] = 600;     // rythme max absolu
		det_inst[44][1] = 45;     //note reference
		det_inst[44][2] = 65;      // note max
		det_inst[44][3] = 31;      // note min
	
		//cordes pizzicato
		det_inst[45][0] = 600;     // rythme max absolu
		det_inst[45][1] = 45;     //note reference
		det_inst[45][2] = 65;      // note max
		det_inst[45][3] = 31;      // note min
	
		//harpe
		det_inst[46][0] = 500;     // rythme max absolu
		det_inst[46][1] = 45;     //note reference
		det_inst[46][2] = 65;      // note max
		det_inst[46][3] = 31;      // note min
	
		//Timbales
		det_inst[47][0] = 600;     // rythme max absolu
		det_inst[47][1] = 45;     //note reference
		det_inst[47][2] = 65;      // note max
		det_inst[47][3] = 31;      // note min
	
		//ensemble acoustique corde 1
		det_inst[48][0] = 500;     // rythme max absolu
		det_inst[48][1] = 45;     //note reference
		det_inst[48][2] = 65;      // note max
		det_inst[48][3] = 31;      // note min
	
		//ensemble acoustique corde 2
		det_inst[49][0] = 400;     // rythme max absolu
		det_inst[49][1] = 45;     //note reference
		det_inst[49][2] = 65;      // note max
		det_inst[49][3] = 31;      // note min
	
		//cordes synthe 1
		det_inst[50][0] = 500;     // rythme max absolu
		det_inst[50][1] = 45;     //note reference
		det_inst[50][2] = 65;      // note max
		det_inst[50][3] = 31;      // note min
	
		//cordes synthe 2
		det_inst[51][0] = 500;     // rythme max absolu
		det_inst[51][1] = 45;     //note reference
		det_inst[51][2] = 65;      // note max
		det_inst[51][3] = 31;      // note min
	
		//coeur aah
		det_inst[52][0] = 400;     // rythme max absolu
		det_inst[52][1] = 45;     //note reference
		det_inst[52][2] = 65;      // note max
		det_inst[52][3] = 31;      // note min
	
		//coeur ooh
		det_inst[53][0] = 400;     // rythme max absolu
		det_inst[53][1] = 45;     //note reference
		det_inst[53][2] = 65;      // note max
		det_inst[53][3] = 31;      // note min
	
		//voix synthe
		det_inst[54][0] = 500;     // rythme max absolu
		det_inst[54][1] = 45;     //note reference
		det_inst[54][2] = 65;      // note max
		det_inst[54][3] = 31;      // note min
		
		//coup d'orchestre
		det_inst[55][0] = 500;     // rythme max absolu
		det_inst[55][1] = 45;     //note reference
		det_inst[55][2] = 65;      // note max
		det_inst[55][3] = 31;      // note min
	
		//trompette
		det_inst[56][0] = 500;     // rythme max absolu
		det_inst[56][1] = 48;     //note reference
		det_inst[56][2] = 64;      // note max
		det_inst[56][3] = 30;      // note min
	
		//trombone
		det_inst[57][0] = 400;     // rythme max absolu
		det_inst[57][1] = 50;     //note reference
		det_inst[57][2] = 67;      // note max
		det_inst[57][3] = 36;      // note min
	
		//tuba
		det_inst[58][0] = 400;     // rythme max absolu
		det_inst[58][1] = 45;     //note reference
		det_inst[58][2] = 65;      // note max
		det_inst[58][3] = 31;      // note min
	
		//trompette sourdine
		det_inst[59][0] = 500;     // rythme max absolu
		det_inst[59][1] = 48;     //note reference
		det_inst[59][2] = 64;      // note max
		det_inst[59][3] = 30;      // note min
	
		//cor d'harmonie
		det_inst[60][0] = 400;     // rythme max absolu
		det_inst[60][1] = 45;     //note reference
		det_inst[60][2] = 65;      // note max
		det_inst[60][3] = 31;      // note min
	
		//section cuivres
		det_inst[61][0] = 400;     // rythme max absolu
		det_inst[61][1] = 45;     //note reference
		det_inst[61][2] = 65;      // note max
		det_inst[61][3] = 31;      // note min
	
		//cuivres synthe 1
		det_inst[62][0] = 400;     // rythme max absolu
		det_inst[62][1] = 45;     //note reference
		det_inst[62][2] = 65;      // note max
		det_inst[62][3] = 31;      // note min
	
		//cuivres synthe 2
		det_inst[63][0] = 400;     // rythme max absolu
		det_inst[63][1] = 45;     //note reference
		det_inst[63][2] = 65;      // note max
		det_inst[63][3] = 31;      // note min
	
		//sax soprano
		det_inst[64][0] = 600;     // rythme max absolu
		det_inst[64][1] = 45;     //note reference
		det_inst[64][2] = 62;      // note max
		det_inst[64][3] = 32;      // note min
	
		//sax alto
		det_inst[65][0] = 600;     // rythme max absolu
		det_inst[65][1] = 40;     //note reference
		det_inst[65][2] = 56;      // note max
		det_inst[65][3] = 25;      // note min
	
		//sax tenor
		det_inst[66][0] = 600;     // rythme max absolu
		det_inst[66][1] = 35;     //note reference
		det_inst[66][2] = 51;      // note max
		det_inst[66][3] = 20;      // note min
	
		//sax baryton
		det_inst[67][0] = 500;     // rythme max absolu
		det_inst[67][1] = 28;     //note reference
		det_inst[67][2] = 44;      // note max
		det_inst[67][3] = 13;      // note min
	
		//hautbois
		det_inst[68][0] = 500;     // rythme max absolu
		det_inst[68][1] = 48;     //note reference
		det_inst[68][2] = 69;      // note max
		det_inst[68][3] = 34;      // note min
	
		//cor anglais
		det_inst[69][0] = 500;     // rythme max absolu
		det_inst[69][1] = 48;     //note reference
		det_inst[69][2] = 65;      // note max
		det_inst[69][3] = 34;      // note min
	
		//basson
		det_inst[70][0] = 500;     // rythme max absolu
		det_inst[70][1] = 28;     //note reference
		det_inst[70][2] = 48;      // note max
		det_inst[70][3] = 10;      // note min
	
		//clarinette
		det_inst[71][0] = 600;     // rythme max absolu
		det_inst[71][1] = 46;     //note reference
		det_inst[71][2] = 67;      // note max
		det_inst[71][3] = 26;      // note min
	
		//picolo
		det_inst[72][0] = 600;     // rythme max absolu
		det_inst[72][1] = 54;     //note reference
		det_inst[72][2] = 69;      // note max
		det_inst[72][3] = 40;      // note min
	
		//flute
		det_inst[73][0] = 600;     // rythme max absolu
		det_inst[73][1] = 48;     //note reference
		det_inst[73][2] = 60;      // note max
		det_inst[73][3] = 36;      // note min
	
		//flute a bec
		det_inst[74][0] = 500;     // rythme max absolu
		det_inst[74][1] = 48;     //note reference
		det_inst[74][2] = 60;      // note max
		det_inst[74][3] = 36;      // note min
	
		//flute de pan
		det_inst[75][0] = 600;     // rythme max absolu
		det_inst[75][1] = 48;     //note reference
		det_inst[75][2] = 67;      // note max
		det_inst[75][3] = 36;      // note min
	
		//bouteille soufle
		det_inst[76][0] = 600;     // rythme max absolu
		det_inst[76][1] = 48;     //note reference
		det_inst[76][2] = 67;      // note max
		det_inst[76][3] = 36;      // note min
	
		//shakuhachi
		det_inst[77][0] = 500;     // rythme max absolu
		det_inst[77][1] = 48;     //note reference
		det_inst[77][2] = 67;      // note max
		det_inst[77][3] = 36;      // note min
	
		//sifflet
		det_inst[78][0] = 600;     // rythme max absolu
		det_inst[78][1] = 48;     //note reference
		det_inst[78][2] = 67;      // note max
		det_inst[78][3] = 36;      // note min
	
		//ocarina
		det_inst[79][0] = 600;     // rythme max absolu
		det_inst[79][1] = 48;     //note reference
		det_inst[79][2] = 67;      // note max
		det_inst[79][3] = 36;      // note min
	
		//signal carre 
		det_inst[80][0] = 600;     // rythme max absolu
		det_inst[80][1] = 47;     //note reference
		det_inst[80][2] = 72;      // note max
		det_inst[80][3] = 24;      // note min
		
		//signal dent de scie 
		det_inst[81][0] = 600;     // rythme max absolu
		det_inst[81][1] = 47;     //note reference
		det_inst[81][2] = 72;      // note max
		det_inst[81][3] = 24;      // note min
	
		//orgue a vapeur 
		det_inst[82][0] = 600;     // rythme max absolu
		det_inst[82][1] = 47;     //note reference
		det_inst[82][2] = 72;      // note max
		det_inst[82][3] = 24;      // note min
	
		//chiffer
		det_inst[83][0] = 600;     // rythme max absolu
		det_inst[83][1] = 47;     //note reference
		det_inst[83][2] = 72;      // note max
		det_inst[83][3] = 24;      // note min
	
		//charang 
		det_inst[84][0] = 600;     // rythme max absolu
		det_inst[84][1] = 47;     //note reference
		det_inst[84][2] = 72;      // note max
		det_inst[84][3] = 24;      // note min
	
		//voix solo
		det_inst[85][0] = 600;     // rythme max absolu
		det_inst[85][1] = 47;     //note reference
		det_inst[85][2] = 72;      // note max
		det_inst[85][3] = 24;      // note min
	
		//signal dent de scie quite 
		det_inst[86][0] = 600;     // rythme max absolu
		det_inst[86][1] = 47;     //note reference
		det_inst[86][2] = 72;      // note max
		det_inst[86][3] = 24;      // note min
	
		//signal basse solo
		det_inst[87][0] = 600;     // rythme max absolu
		det_inst[87][1] = 47;     //note reference
		det_inst[87][2] = 72;      // note max
		det_inst[87][3] = 24;      // note min
	
		//fantaisie 
		det_inst[88][0] = 600;     // rythme max absolu
		det_inst[88][1] = 47;     //note reference
		det_inst[88][2] = 72;      // note max
		det_inst[88][3] = 24;      // note min
	
		//son chaleureux
		det_inst[89][0] = 600;     // rythme max absolu
		det_inst[89][1] = 47;     //note reference
		det_inst[89][2] = 72;      // note max
		det_inst[89][3] = 24;      // note min
	
		//polysynthe 
		det_inst[90][0] = 600;     // rythme max absolu
		det_inst[90][1] = 47;     //note reference
		det_inst[90][2] = 72;      // note max
		det_inst[90][3] = 24;      // note min
	
		//choeur 
		det_inst[91][0] = 600;     // rythme max absolu
		det_inst[91][1] = 47;     //note reference
		det_inst[91][2] = 72;      // note max
		det_inst[91][3] = 24;      // note min
	
		//archet 
		det_inst[92][0] = 400;     // rythme max absolu
		det_inst[92][1] = 47;     //note reference
		det_inst[92][2] = 72;      // note max
		det_inst[92][3] = 24;      // note min
	
		//métallique
		det_inst[93][0] = 600;     // rythme max absolu
		det_inst[93][1] = 47;     //note reference
		det_inst[93][2] = 72;      // note max
		det_inst[93][3] = 24;      // note min
	
		//halo
		det_inst[94][0] = 600;     // rythme max absolu
		det_inst[94][1] = 47;     //note reference
		det_inst[94][2] = 72;      // note max
		det_inst[94][3] = 24;      // note min
	
		//balai
		det_inst[95][0] = 600;     // rythme max absolu
		det_inst[95][1] = 47;     //note reference
		det_inst[95][2] = 72;      // note max
		det_inst[95][3] = 24;      // note min
	
		//pluie de glace
		det_inst[96][0] = 600;     // rythme max absolu
		det_inst[96][1] = 47;     //note reference
		det_inst[96][2] = 72;      // note max
		det_inst[96][3] = 24;      // note min
	
		//trames sonores
		det_inst[97][0] = 600;     // rythme max absolu
		det_inst[97][1] = 47;     //note reference
		det_inst[97][2] = 72;      // note max
		det_inst[97][3] = 24;      // note min
	
		//cristal
		det_inst[98][0] = 600;     // rythme max absolu
		det_inst[98][1] = 47;     //note reference
		det_inst[98][2] = 72;      // note max
		det_inst[98][3] = 24;      // note min
	
		//atmosphere
		det_inst[99][0] = 600;     // rythme max absolu
		det_inst[99][1] = 47;     //note reference
		det_inst[99][2] = 72;      // note max
		det_inst[99][3] = 24;      // note min
	
		//brillance 
		det_inst[100][0] = 600;     // rythme max absolu
		det_inst[100][1] = 47;     //note reference
		det_inst[100][2] = 72;      // note max
		det_inst[100][3] = 24;      // note min
	
		//gobelins
		det_inst[101][0] = 600;     // rythme max absolu
		det_inst[101][1] = 47;     //note reference
		det_inst[101][2] = 72;      // note max
		det_inst[101][3] = 24;      // note min
	
		//echos 
		det_inst[102][0] = 600;     // rythme max absolu
		det_inst[102][1] = 47;     //note reference
		det_inst[102][2] = 72;      // note max
		det_inst[102][3] = 24;      // note min
	
		//espace sci-fi
		det_inst[103][0] = 600;     // rythme max absolu
		det_inst[103][1] = 47;     //note reference
		det_inst[103][2] = 72;      // note max
		det_inst[103][3] = 24;      // note min
	
		//sitar 
		det_inst[104][0] = 600;     // rythme max absolu
		det_inst[104][1] = 47;     //note reference
		det_inst[104][2] = 72;      // note max
		det_inst[104][3] = 24;      // note min
	
		//banjo
		det_inst[105][0] = 600;     // rythme max absolu
		det_inst[105][1] = 47;     //note reference
		det_inst[105][2] = 72;      // note max
		det_inst[105][3] = 24;      // note min
	
		//shamisen
		det_inst[106][0] = 600;     // rythme max absolu
		det_inst[106][1] = 47;     //note reference
		det_inst[106][2] = 72;      // note max
		det_inst[106][3] = 24;      // note min
	
		//koto
		det_inst[107][0] = 600;     // rythme max absolu
		det_inst[107][1] = 47;     //note reference
		det_inst[107][2] = 72;      // note max
		det_inst[107][3] = 24;      // note min
	
		//kalimba
		det_inst[108][0] = 600;     // rythme max absolu
		det_inst[108][1] = 47;     //note reference
		det_inst[108][2] = 72;      // note max
		det_inst[108][3] = 24;      // note min
	
		//cornemuse 
		det_inst[109][0] = 600;     // rythme max absolu
		det_inst[109][1] = 47;     //note reference
		det_inst[109][2] = 72;      // note max
		det_inst[109][3] = 24;      // note min
	
		//viole
		det_inst[110][0] = 500;     // rythme max absolu
		det_inst[110][1] = 47;     //note reference
		det_inst[110][2] = 72;      // note max
		det_inst[110][3] = 24;      // note min
	
		//shehnai
		det_inst[111][0] = 600;     // rythme max absolu
		det_inst[111][1] = 47;     //note reference
		det_inst[111][2] = 72;      // note max
		det_inst[111][3] = 24;      // note min
	
		//clochettes
	
		det_inst[112][0] = 600;     // rythme max absolu
		det_inst[112][1] = 47;     //note reference
		det_inst[112][2] = 72;      // note max
		det_inst[112][3] = 24;      // note min
	
		//agogo
		det_inst[113][0] = 600;     // rythme max absolu
		det_inst[113][1] = 47;     //note reference
		det_inst[113][2] = 72;      // note max
		det_inst[113][3] = 24;      // note min
	
		//batterie metallique
		det_inst[114][0] = 600;     // rythme max absolu
		det_inst[114][1] = 47;     //note reference
		det_inst[114][2] = 72;      // note max
		det_inst[114][3] = 24;      // note min
	
		//planchettes
		det_inst[115][0] = 600;     // rythme max absolu
		det_inst[115][1] = 47;     //note reference
		det_inst[115][2] = 72;      // note max
		det_inst[115][3] = 24;      // note min
	
		//timbales
		det_inst[116][0] = 600;     // rythme max absolu
		det_inst[116][1] = 24;     //note reference
		det_inst[116][2] = 40;      // note max
		det_inst[116][3] = 10;      // note min
	
		//tom mélodique
		det_inst[117][0] = 600;     // rythme max absolu
		det_inst[117][1] = 47;     //note reference
		det_inst[117][2] = 72;      // note max
		det_inst[117][3] = 24;      // note min
	
		//tambour synthetique
		det_inst[118][0] = 600;     // rythme max absolu
		det_inst[118][1] = 47;     //note reference
		det_inst[118][2] = 72;      // note max
		det_inst[118][3] = 24;      // note min
	
		//cymbale
		det_inst[119][0] = 600;     // rythme max absolu
		det_inst[119][1] = 47;     //note reference
		det_inst[119][2] = 72;      // note max
		det_inst[119][3] = 24;      // note min
	
		//guitare bruit de frette
		det_inst[120][0] = 600;     // rythme max absolu
		det_inst[120][1] = 47;     //note reference
		det_inst[120][2] = 72;      // note max
		det_inst[120][3] = 24;      // note min
	
		//respiration
		det_inst[121][0] = 600;     // rythme max absolu
		det_inst[121][1] = 47;     //note reference
		det_inst[121][2] = 72;      // note max
		det_inst[121][3] = 24;      // note min
	
		//rivage
		det_inst[122][0] = 300;     // rythme max absolu
		det_inst[122][1] = 47;     //note reference
		det_inst[122][2] = 72;      // note max
		det_inst[122][3] = 24;      // note min
	
		//gazouilli
		det_inst[123][0] = 600;     // rythme max absolu
		det_inst[123][1] = 47;     //note reference
		det_inst[123][2] = 72;      // note max
		det_inst[123][3] = 24;      // note min
	
		//sonnerie tel
		det_inst[124][0] = 600;     // rythme max absolu
		det_inst[124][1] = 47;     //note reference
		det_inst[124][2] = 72;      // note max
		det_inst[124][3] = 24;      // note min
	
		//hélicoptere
		det_inst[125][0] = 300;     // rythme max absolu
		det_inst[125][1] = 47;     //note reference
		det_inst[125][2] = 72;      // note max
		det_inst[125][3] = 24;      // note min
	
		//applaudissements
		det_inst[126][0] = 300;     // rythme max absolu
		det_inst[126][1] = 47;     //note reference
		det_inst[126][2] = 72;      // note max
		det_inst[126][3] = 24;      // note min
	
		//coup de feu
		det_inst[127][0] = 600;     // rythme max absolu
		det_inst[127][1] = 47;     //note reference
		det_inst[127][2] = 72;      // note max
		det_inst[127][3] = 24;      // note min
	
		// Liste de la voie basse des instruments
		
		//piano grand acoustique voie basse
		det_inst[128][0] = 600;     // rythme max absolu
		det_inst[128][1] = 20;     //note reference
		det_inst[128][2] = 36;      // note max
		det_inst[128][3] = 10;      // note min
		
		//piano acoustique
		det_inst[129][0] = 600;     // rythme max absolu
		det_inst[129][1] = 20;     //note reference
		det_inst[129][2] = 36;      // note max
		det_inst[129][3] = 10;      // note min
	
		//piano grand electrique
		det_inst[130][0] = 600;     // rythme max absolu
		det_inst[130][1] = 20;     //note reference
		det_inst[130][2] = 36;      // note max
		det_inst[130][3] = 10;      // note min
	
		//piano bastringue
		det_inst[131][0] = 600;     // rythme max absolu
		det_inst[131][1] = 20;     //note reference
		det_inst[131][2] = 36;      // note max
		det_inst[131][3] = 10;      // note min
	
		//piano electrique
		det_inst[132][0] = 600;     // rythme max absolu
		det_inst[132][1] = 20;     //note reference
		det_inst[132][2] = 36;      // note max
		det_inst[132][3] = 10;      // note min
	
		//piano effet chorus
		det_inst[133][0] = 400;     // rythme max absolu
		det_inst[133][1] = 20;     //note reference
		det_inst[133][2] = 36;      // note max
		det_inst[133][3] = 10;      // note min
	
		//clavecin
		det_inst[134][0] = 500;     // rythme max absolu
		det_inst[134][1] = 20;     //note reference
		det_inst[134][2] = 36;      // note max
		det_inst[134][3] = 10;      // note min
	
		//clavinet
		det_inst[135][0] = 500;     // rythme max absolu
		det_inst[135][1] = 20;     //note reference
		det_inst[135][2] = 36;      // note max
		det_inst[135][3] = 10;      // note min
	
		//celesta
		det_inst[136][0] = 800;     // rythme max absolu
		det_inst[136][1] = 20;     //note reference
		det_inst[136][2] = 36;      // note max
		det_inst[136][3] = 10;      // note min
	
		//Glockenspiel
		det_inst[140][0] = 800;     // rythme max absolu
		det_inst[140][1] = 20;     //note reference
		det_inst[140][2] = 36;      // note max
		det_inst[140][3] = 10;      // note min
	
		//boite a musique
		det_inst[141][0] = 800;     // rythme max absolu
		det_inst[141][1] = 20;     //note reference
		det_inst[141][2] = 36;      // note max
		det_inst[141][3] = 10;      // note min
	
		//Vibraphone
		det_inst[142][0] = 800;     // rythme max absolu
		det_inst[142][1] = 20;     //note reference
		det_inst[142][2] = 36;      // note max
		det_inst[142][3] = 10;      // note min
	
		//marimba
		det_inst[143][0] = 800;     // rythme max absolu
		det_inst[143][1] = 20;     //note reference
		det_inst[143][2] = 36;      // note max
		det_inst[143][3] = 10;      // note min
	
		//Xylophone
		det_inst[144][0] = 800;     // rythme max absolu
		det_inst[144][1] = 20;     //note reference
		det_inst[144][2] = 36;      // note max
		det_inst[144][3] = 10;      // note min
	
		//cloches tubulaires
		det_inst[145][0] = 800;     // rythme max absolu
		det_inst[145][1] = 20;     //note reference
		det_inst[145][2] = 36;      // note max
		det_inst[145][3] = 10;      // note min
	
		//Tympanon
		det_inst[146][0] = 800;     // rythme max absolu
		det_inst[146][1] = 20;     //note reference
		det_inst[146][2] = 36;      // note max
		det_inst[146][3] = 10;      // note min
	
		//Orgue hammond
		det_inst[147][0] = 500;     // rythme max absolu
		det_inst[147][1] = 20;     //note reference
		det_inst[147][2] = 36;      // note max
		det_inst[147][3] = 10;      // note min
	
		//Orgue a percussion
		det_inst[148][0] = 500;     // rythme max absolu
		det_inst[148][1] = 20;     //note reference
		det_inst[148][2] = 36;      // note max
		det_inst[148][3] = 10;      // note min
	
		//Orgue Rock
		det_inst[149][0] = 500;     // rythme max absolu
		det_inst[149][1] = 20;     //note reference
		det_inst[149][2] = 36;      // note max
		det_inst[149][3] = 10;      // note min
	
		//grandes orgues
		det_inst[150][0] = 300;     // rythme max absolu
		det_inst[150][1] = 20;     //note reference
		det_inst[150][2] = 36;      // note max
		det_inst[150][3] = 10;      // note min
	
		//harmonium
		det_inst[151][0] = 300;     // rythme max absolu
		det_inst[151][1] = 20;     //note reference
		det_inst[151][2] = 36;      // note max
		det_inst[151][3] = 10;      // note min
	
		//accordeon
		det_inst[152][0] = 600;     // rythme max absolu
		det_inst[152][1] = 20;     //note reference
		det_inst[152][2] = 36;      // note max
		det_inst[152][3] = 10;      // note min
	
		//Harmonica
		det_inst[153][0] = 400;     // rythme max absolu
		det_inst[153][1] = 20;     //note reference
		det_inst[153][2] = 36;      // note max
		det_inst[153][3] = 10;      // note min
	
		//accordeon tango
		det_inst[154][0] = 600;     // rythme max absolu
		det_inst[154][1] = 20;     //note reference
		det_inst[154][2] = 36;      // note max
		det_inst[154][3] = 10;      // note min
	
	}

}
