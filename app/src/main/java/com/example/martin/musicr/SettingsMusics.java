package com.example.martin.musicr;

/**
 * Created by martin on 14/01/2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.example.martin.musicr.R;


public class SettingsMusics extends Fragment{

    private Switch switchRingtone;
    private RelativeLayout layoutRingtone;

    private Switch switchSnooze;
    private RelativeLayout layoutSnooze;

    private TextView textInstPstatRingtone;
    private TextView textInstPRingtone;
    private SeekBar seekBarInstPRingtone;

    private TextView textInstSstatRingtone;
    private TextView textInstSRingtone;
    private SeekBar seekBarInstSRingtone;

    private TextView textTimestatRingtone;
    private TextView textTimeRingtone;
    private SeekBar seekBarTimeRingtone;

    private TextView textTonalitystatRingtone;
    private SeekBar seekBarTonalityRingtone;

    private TextView textTonality2statRingtone;
    private SeekBar seekBarTonality2Ringtone;

    private TextView textTempostatRingtone;
    private TextView textTempoRingtone;
    private SeekBar seekBarTempoRingtone;

    private int nbreInstPRingtone;
    private int nbreInstSRingtone;
    private int nbreTimeRingtone;
    private int nbreTonalityRingtone;
    private int nbreTempoRingtone;
    private int nbreTonality2Ringtone;

    private TextView textInstPstatSnooze;
    private TextView textInstPSnooze;
    private SeekBar seekBarInstPSnooze;

    private TextView textInstSstatSnooze;
    private TextView textInstSSnooze;
    private SeekBar seekBarInstSSnooze;

    private TextView textTimestatSnooze;
    private TextView textTimeSnooze;
    private SeekBar seekBarTimeSnooze;

    private TextView textTonalitystatSnooze;
    private SeekBar seekBarTonalitySnooze;

    private TextView textTonality2statSnooze;
    private SeekBar seekBarTonality2Snooze;

    private TextView textTempostatSnooze;
    private TextView textTempoSnooze;
    private SeekBar seekBarTempoSnooze;

    private int nbreInstPSnooze;
    private int nbreInstSSnooze;
    private int nbreTimeSnooze;
    private int nbreTonalitySnooze;
    private int nbreTempoSnooze;
    private int nbreTonality2Snooze;

    SharedPreferences sharedpreferences;
    SharedPreferences sharedpreferencesSwitch;
    SharedPreferences sharedpreferencesRingtone;
    SharedPreferences sharedpreferencesSnooze;

    public SettingsMusics() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_settings, container, false);

        switchRingtone = (Switch) view.findViewById(R.id.switch2);
        layoutRingtone = (RelativeLayout) view.findViewById(R.id.layoutRingtone);

        switchSnooze = (Switch) view.findViewById(R.id.switch1);
        layoutSnooze = (RelativeLayout) view.findViewById(R.id.layoutSnooze);

        textInstPstatRingtone  = (TextView) view.findViewById(R.id.textView28);
        textInstPRingtone  = (TextView) view.findViewById(R.id.textView29);
        seekBarInstPRingtone = (SeekBar) view.findViewById(R.id.seekBar9);

        textInstSstatRingtone = (TextView) view.findViewById(R.id.textView36);
        textInstSRingtone = (TextView) view.findViewById(R.id.textView37);
        seekBarInstSRingtone = (SeekBar) view.findViewById(R.id.seekBar13);

        textTimestatRingtone = (TextView) view.findViewById(R.id.textView38);
        textTimeRingtone = (TextView) view.findViewById(R.id.textView39);
        seekBarTimeRingtone = (SeekBar) view.findViewById(R.id.seekBar14);

        textTonalitystatRingtone = (TextView) view.findViewById(R.id.textView34);
        seekBarTonalityRingtone = (SeekBar) view.findViewById(R.id.seekBar12);

        textTonality2statRingtone = (TextView) view.findViewById(R.id.textView32);
        seekBarTonality2Ringtone = (SeekBar) view.findViewById(R.id.seekBar11);

        textTempostatRingtone = (TextView) view.findViewById(R.id.textView30);
        textTempoRingtone = (TextView) view.findViewById(R.id.textView31);
        seekBarTempoRingtone = (SeekBar) view.findViewById(R.id.seekBar10);

        textInstPstatSnooze  = (TextView) view.findViewById(R.id.textView24);
        textInstPSnooze  = (TextView) view.findViewById(R.id.textView25);
        seekBarInstPSnooze = (SeekBar) view.findViewById(R.id.seekBar7);

        textInstSstatSnooze = (TextView) view.findViewById(R.id.textView10);
        textInstSSnooze = (TextView) view.findViewById(R.id.textView19);
        seekBarInstSSnooze = (SeekBar) view.findViewById(R.id.seekBar4);

        textTimestatSnooze = (TextView) view.findViewById(R.id.textView4);
        textTimeSnooze = (TextView) view.findViewById(R.id.textView5);
        seekBarTimeSnooze = (SeekBar) view.findViewById(R.id.seekBar);

        textTonalitystatSnooze = (TextView) view.findViewById(R.id.textView20);
        seekBarTonalitySnooze = (SeekBar) view.findViewById(R.id.seekBar5);

        textTonality2statSnooze = (TextView) view.findViewById(R.id.textView22);
        seekBarTonality2Snooze = (SeekBar) view.findViewById(R.id.seekBar6);

        textTempostatSnooze = (TextView) view.findViewById(R.id.textView26);
        textTempoSnooze = (TextView) view.findViewById(R.id.textView27);
        seekBarTempoSnooze = (SeekBar) view.findViewById(R.id.seekBar8);

        activationSwitch();
        initializeRingtone();
        initializeSnooze();

        // Inflate the layout for this fragment
        return view;
    }

    private void initializeRingtone() {
        sharedpreferences = getContext().getSharedPreferences("generateRingtone", Context.MODE_PRIVATE);
        sharedpreferencesRingtone = getContext().getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);

        if(sharedpreferencesRingtone.getInt("instP",0) == 0)
        {
            textInstPstatRingtone.setText("random");
            textInstPRingtone.setVisibility(TextView.GONE);
            seekBarInstPRingtone.setProgress(0);
        }
        else
        {
            seekBarInstPRingtone.setProgress(sharedpreferences.getInt("nbreInstP", 0));
            textInstPRingtone.setText(String.valueOf(sharedpreferences.getInt("nbreInstP", 0)));
            textInstPstatRingtone.setText("/ 4");
            textInstPRingtone.setVisibility(TextView.VISIBLE);
        }

        if(sharedpreferencesRingtone.getInt("instS",0) == 0)
        {
            textInstSstatRingtone.setText("random");
            textInstSRingtone.setVisibility(TextView.GONE);
            seekBarInstSRingtone.setProgress(0);
        }
        else
        {
            seekBarInstSRingtone.setProgress(sharedpreferences.getInt("nbreInstS", 0));
            textInstSRingtone.setText(String.valueOf(sharedpreferences.getInt("nbreInstS", 0)));
            textInstSstatRingtone.setText("/ 4");
            textInstSRingtone.setVisibility(TextView.VISIBLE);
        }

        if(sharedpreferencesRingtone.getInt("time",0) == 0)
        {
            textTimestatRingtone.setText("random");
            textTimeRingtone.setVisibility(TextView.GONE);
            seekBarTimeRingtone.setProgress(0);
        }
        else
        {
            seekBarTimeRingtone.setProgress(sharedpreferences.getInt("nbreTime", 0));
            textTimeRingtone.setText(String.valueOf(sharedpreferences.getInt("nbreTime", 0)));
            textTimestatRingtone.setText("/ 270");
            textTimeRingtone.setVisibility(TextView.VISIBLE);
        }

        if(sharedpreferencesRingtone.getInt("tonality",0) == 0)
        {
            textTonalitystatRingtone.setText("random");
            seekBarTonalityRingtone.setProgress(0);
        }
        else
        {
            seekBarTonalityRingtone.setProgress(sharedpreferences.getInt("nbreTonality", 0));
            textTonalitystatRingtone.setText(String.valueOf(sharedpreferences.getInt("nbreTonality", 0)));
        }

        if(sharedpreferencesRingtone.getInt("tempo",0) == 0)
        {
            textTempostatRingtone.setText("random");
            textTempoRingtone.setVisibility(TextView.GONE);
            seekBarTempoRingtone.setProgress(0);
        }
        else
        {
            seekBarTempoRingtone.setProgress(sharedpreferences.getInt("nbreTempo", 0));
            textTempoRingtone.setText(String.valueOf(sharedpreferences.getInt("nbreTempo", 0)));
            textTempostatRingtone.setText("/ 200 bpm");
            textTempoRingtone.setVisibility(TextView.VISIBLE);
        }

        if(sharedpreferencesRingtone.getInt("tonality2",0) == 0)
        {
            textTonality2statRingtone.setText("random");
            seekBarTonality2Ringtone.setProgress(0);
        }
        else
        {
            seekBarTonality2Ringtone.setProgress(sharedpreferences.getInt("nbreTonality2", 0));
            textTonality2statRingtone.setText(String.valueOf(sharedpreferences.getInt("nbreTonality2", 0)));
        }

        seekBarInstPRingtone.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub
                if (progress == 0) {
                    textInstPstatRingtone.setText("random");
                    textInstPRingtone.setVisibility(TextView.GONE);
                    sharedpreferences = getContext().getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("instP", 0);
                    editor.commit();
                } else {
                    nbreInstPRingtone = progress;
                    textInstPstatRingtone.setText("/ 4");
                    textInstPRingtone.setVisibility(TextView.VISIBLE);
                    textInstPRingtone.setText(String.valueOf(nbreInstPRingtone));
                    sharedpreferences = getContext().getSharedPreferences("generateRingtone", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreInstP", nbreInstPRingtone);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("instP", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarInstSRingtone.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub

                if (progress == 0) {
                    textInstSstatRingtone.setText("random");
                    textInstSRingtone.setVisibility(TextView.GONE);
                    sharedpreferences = getContext().getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("instS", 0);
                    editor.commit();
                } else {
                    nbreInstSRingtone = progress -1;
                    textInstSstatRingtone.setText("/ 4");
                    textInstSRingtone.setVisibility(TextView.VISIBLE);
                    textInstSRingtone.setText(String.valueOf(nbreInstSRingtone));
                    sharedpreferences = getContext().getSharedPreferences("generateRingtone", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreInstS", nbreInstSRingtone);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("instS", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarTimeRingtone.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    textTimestatRingtone.setText("random");
                    textTimeRingtone.setVisibility(TextView.GONE);
                    sharedpreferences = getContext().getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("time", 0);
                    editor.commit();
                } else {
                    nbreTimeRingtone = progress + 99;
                    textTimestatRingtone.setText("/ 270");
                    textTimeRingtone.setVisibility(TextView.VISIBLE);
                    textTimeRingtone.setText(String.valueOf(nbreTimeRingtone));
                    sharedpreferences = getContext().getSharedPreferences("generateRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreTime", nbreTimeRingtone);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("time", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarTonality2Ringtone.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    textTonality2statRingtone.setText("random");
                    sharedpreferences = getContext().getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("tonality2", 0);
                    editor.commit();
                } else {
                    if(progress == 1)
                    {
                        textTonality2statRingtone.setText("Major");
                    }
                    if(progress == 2) {
                        textTonality2statRingtone.setText("Minor");
                    }
                    nbreTonality2Ringtone = progress;

                    sharedpreferences = getContext().getSharedPreferences("generateRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreTonality2", nbreTonality2Ringtone);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("tonality2", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarTonalityRingtone.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    textTonalitystatRingtone.setText("random");
                    sharedpreferences = getContext().getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("tonality", 0);
                    editor.commit();
                } else {
                    nbreTonalityRingtone = progress;
                    textTonalitystatRingtone.setText(String.valueOf(nbreTonalityRingtone));
                    sharedpreferences = getContext().getSharedPreferences("generateRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreTonality", nbreTonalityRingtone);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("tonality", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarTempoRingtone.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    textTempostatRingtone.setText("random");
                    textTempoRingtone.setVisibility(TextView.GONE);
                    sharedpreferences = getContext().getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("tempo", 0);
                    editor.commit();
                } else {
                    nbreTempoRingtone = progress + 39;
                    textTempostatRingtone.setText("/ 200 bpm");
                    textTempoRingtone.setVisibility(TextView.VISIBLE);
                    textTempoRingtone.setText(String.valueOf(nbreTempoRingtone));
                    sharedpreferences = getContext().getSharedPreferences("generateRingtone", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreTempo", nbreTempoRingtone);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settingsRingtone", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("tempo", 1);
                    editorbis.commit();
                }
            }
        });
    }

    private void initializeSnooze() {
        sharedpreferences = getContext().getSharedPreferences("generateSnooze", Context.MODE_PRIVATE);
        sharedpreferencesSnooze = getContext().getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);

        if(sharedpreferencesSnooze.getInt("instP",0) == 0)
        {
            textInstPstatSnooze.setText("random");
            textInstPSnooze.setVisibility(TextView.GONE);
            seekBarInstPSnooze.setProgress(0);
        }
        else
        {
            seekBarInstPSnooze.setProgress(sharedpreferences.getInt("nbreInstP", 0));
            textInstPSnooze.setText(String.valueOf(sharedpreferences.getInt("nbreInstP", 0)));
            textInstPstatSnooze.setText("/ 4");
            textInstPSnooze.setVisibility(TextView.VISIBLE);
        }

        if(sharedpreferencesSnooze.getInt("instS",0) == 0)
        {
            textInstSstatSnooze.setText("random");
            textInstSSnooze.setVisibility(TextView.GONE);
            seekBarInstSSnooze.setProgress(0);
        }
        else
        {
            seekBarInstSSnooze.setProgress(sharedpreferences.getInt("nbreInstS", 0));
            textInstSSnooze.setText(String.valueOf(sharedpreferences.getInt("nbreInstS", 0)));
            textInstSstatSnooze.setText("/ 4");
            textInstSSnooze.setVisibility(TextView.VISIBLE);
        }

        if(sharedpreferencesSnooze.getInt("time",0) == 0)
        {
            textTimestatSnooze.setText("random");
            textTimeSnooze.setVisibility(TextView.GONE);
            seekBarTimeSnooze.setProgress(0);
        }
        else
        {
            seekBarTimeSnooze.setProgress(sharedpreferences.getInt("nbreTime", 0));
            textTimeSnooze.setText(String.valueOf(sharedpreferences.getInt("nbreTime", 0)));
            textTimestatSnooze.setText("/ 270");
            textTimeSnooze.setVisibility(TextView.VISIBLE);
        }

        if(sharedpreferencesSnooze.getInt("tonality",0) == 0)
        {
            textTonalitystatSnooze.setText("random");
            seekBarTonalitySnooze.setProgress(0);
        }
        else
        {
            seekBarTonalitySnooze.setProgress(sharedpreferences.getInt("nbreTonality", 0));
            textTonalitystatSnooze.setText(String.valueOf(sharedpreferences.getInt("nbreTonality", 0)));
        }

        if(sharedpreferencesSnooze.getInt("tempo",0) == 0)
        {
            textTempostatSnooze.setText("random");
            textTempoSnooze.setVisibility(TextView.GONE);
            seekBarTempoSnooze.setProgress(0);
        }
        else
        {
            seekBarTempoSnooze.setProgress(sharedpreferences.getInt("nbreTempo", 0));
            textTempoSnooze.setText(String.valueOf(sharedpreferences.getInt("nbreTempo", 0)));
            textTempostatSnooze.setText("/ 200 bpm");
            textTempoSnooze.setVisibility(TextView.VISIBLE);
        }

        if(sharedpreferencesSnooze.getInt("tonality2",0) == 0)
        {
            textTonality2statSnooze.setText("random");
            seekBarTonality2Snooze.setProgress(0);
        }
        else
        {
            seekBarTonality2Snooze.setProgress(sharedpreferences.getInt("nbreTonality2", 0));
            textTonality2statSnooze.setText(String.valueOf(sharedpreferences.getInt("nbreTonality2", 0)));
        }

        seekBarInstPSnooze.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub
                if (progress == 0) {
                    textInstPstatSnooze.setText("random");
                    textInstPSnooze.setVisibility(TextView.GONE);
                    sharedpreferences = getContext().getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("binstP", 0);
                    editor.commit();
                } else {
                    nbreInstPSnooze = progress;
                    textInstPstatSnooze.setText("/ 4");
                    textInstPSnooze.setVisibility(TextView.VISIBLE);
                    textInstPSnooze.setText(String.valueOf(nbreInstPSnooze));
                    sharedpreferences = getContext().getSharedPreferences("generateSnooze", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreInstP", nbreInstPSnooze);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("binstP", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarInstSSnooze.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub

                if (progress == 0) {
                    textInstSstatSnooze.setText("random");
                    textInstSSnooze.setVisibility(TextView.GONE);
                    sharedpreferences = getContext().getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("binstS", 0);
                    editor.commit();
                } else {
                    nbreInstSSnooze = progress -1;
                    textInstSstatSnooze.setText("/ 4");
                    textInstSSnooze.setVisibility(TextView.VISIBLE);
                    textInstSSnooze.setText(String.valueOf(nbreInstSSnooze));
                    sharedpreferences = getContext().getSharedPreferences("generateSnooze", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreInstS", nbreInstSSnooze);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("binstS", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarTimeSnooze.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    textTimestatSnooze.setText("random");
                    textTimeSnooze.setVisibility(TextView.GONE);
                    sharedpreferences = getContext().getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("time", 0);
                    editor.commit();
                } else {
                    nbreTimeSnooze = progress + 99;
                    textTimestatSnooze.setText("/ 270");
                    textTimeSnooze.setVisibility(TextView.VISIBLE);
                    textTimeSnooze.setText(String.valueOf(nbreTimeSnooze));
                    sharedpreferences = getContext().getSharedPreferences("generateSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreTime", nbreTimeSnooze);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("time", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarTonality2Snooze.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    textTonality2statSnooze.setText("random");
                    sharedpreferences = getContext().getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("tonality2", 0);
                    editor.commit();
                } else {
                    if(progress == 1)
                    {
                        textTonality2statSnooze.setText("Major");
                    }
                    if(progress == 2) {
                        textTonality2statSnooze.setText("Minor");
                    }
                    nbreTonality2Snooze = progress;

                    sharedpreferences = getContext().getSharedPreferences("generateSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreTonality2", nbreTonality2Snooze);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("tonality2", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarTonalitySnooze.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    textTonalitystatSnooze.setText("random");
                    sharedpreferences = getContext().getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("tonality", 0);
                    editor.commit();
                } else {
                    nbreTonalitySnooze = progress;
                    textTonalitystatSnooze.setText(String.valueOf(nbreTonalitySnooze));
                    sharedpreferences = getContext().getSharedPreferences("generateSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreTonality", nbreTonalitySnooze);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("tonality", 1);
                    editorbis.commit();
                }
            }
        });

        seekBarTempoSnooze.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                //int inVal = Integer.parseInt(String.valueOf(seekBar.getProgress()));
                //inVal =+ 70;
                //Toast.makeText(getApplicationContext(), String.valueOf(inVal),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    textTempostatSnooze.setText("random");
                    textTempoSnooze.setVisibility(TextView.GONE);
                    sharedpreferences = getContext().getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("tempo", 0);
                    editor.commit();
                } else {
                    nbreTempoSnooze = progress + 39;
                    textTempostatSnooze.setText("/ 200 bpm");
                    textTempoSnooze.setVisibility(TextView.VISIBLE);
                    textTempoSnooze.setText(String.valueOf(nbreTempoSnooze));
                    sharedpreferences = getContext().getSharedPreferences("generateSnooze", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("nbreTempo", nbreTempoSnooze);
                    editor.commit();

                    sharedpreferences = getContext().getSharedPreferences("settingsSnooze", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editorbis = sharedpreferences.edit();
                    editorbis.putInt("tempo", 1);
                    editorbis.commit();
                }
            }
        });
    }

    private void activationSwitch()
    {
     //   layoutRingtone.getLayoutParams().height = 80;
     //   layoutSnooze.getLayoutParams().height = 80;

        // Gets linearlayout
       // LinearLayout layout = (LinearLayout)findViewById(R.id.numberPadLayout);
// Gets the layout params that will allow you to resize the layout
        ViewGroup.LayoutParams params = layoutRingtone.getLayoutParams();
// Changes the height and width to the specified *pixels*


        sharedpreferencesSwitch = getContext().getSharedPreferences("activation", Context.MODE_PRIVATE);

        if(sharedpreferencesSwitch.getBoolean("ringtone", true))
        {
            switchRingtone.setChecked(true);
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutRingtone.setLayoutParams(params);
        }
        else
        {
            switchRingtone.setChecked(false);
            params.height = 80;
            layoutRingtone.setLayoutParams(params);
        }

        if(sharedpreferencesSwitch.getBoolean("snooze", true))
        {
            switchSnooze.setChecked(true);
        }
        else
        {
            switchSnooze.setChecked(false);
        }

        switchRingtone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isChecked) {
                  //  layoutRingtone.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
                  //  layoutRingtone.getLayoutParams().height = 80;
                    sharedpreferences = getContext().getSharedPreferences("activation", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean("ringtone", true);
                    editor.commit();
                } else {
                 //   layoutRingtone.getLayoutParams().height = 320;
                    sharedpreferences = getContext().getSharedPreferences("activation", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean("ringtone", false);
                    editor.commit();
                }
            }
        });

        switchSnooze.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isChecked) {
                 //   layoutSnooze.getLayoutParams().height = 80;
                    sharedpreferences = getContext().getSharedPreferences("activation", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean("snooze", true);
                    editor.commit();
                } else {
                //    layoutSnooze.getLayoutParams().height = 320;
                    sharedpreferences = getContext().getSharedPreferences("activation", Context.MODE_PRIVATE);
                    //  compt = sharedpreferences.getInt("nbre", 0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean("snooze", false);
                    editor.commit();
                }
            }
        });
    }

}
