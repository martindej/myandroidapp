package com.example.martin.musicr;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by martin on 27/12/2015.
 */
public class RingtoneHelper {

    Thread SOK_1_CLIENT = null;

    public static List<Ringtone> fetchAvailableRingtones(Context context){

        List<Ringtone> ringtones = new ArrayList<>();
        RingtoneManager mgr = new RingtoneManager(context);
        mgr.setType(RingtoneManager.TYPE_RINGTONE);

        int n = mgr.getCursor().getCount();
        for(int i=0;i<n;i++){
            ringtones.add(mgr.getRingtone(i));
        }

        return  ringtones;
    }

    public static void changeRingtone(Context context){

        SharedPreferences preferences = context.getSharedPreferences("activation", Context.MODE_PRIVATE);
        if(!preferences.getBoolean("ringtone", false))
            return;

  /*      SOK_1_CLIENT = new Thread(new SOK_1_CLIENT(context.getApplicationContext(),-1,-1,0,0,0,0));
        // SOK_1_CLIENT.setContext(getApplicationContext());
        SOK_1_CLIENT.start();*/

     /*   RingtoneManager mgr = new RingtoneManager(context);
        Random random = new Random(System.currentTimeMillis());

        int n = random.nextInt(mgr.getCursor().getCount());

        RingtoneManager.setActualDefaultRingtoneUri(context,
                RingtoneManager.TYPE_RINGTONE, mgr.getRingtoneUri(n));*/
    }
}
