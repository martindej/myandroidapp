package com.example.martin.musicr;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Path;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Created by martin on 04/01/2016.
 */
public class SOK_1_CLIENT implements Runnable {

    String test;
    String nameFichier;
    private TextView textP;
    FileOutputStream fileM = null;
    private Context context;
    private int nbreTime1;
    private int nbreTime2;
    private int nbreTempo1;
    private int nbreTempo2;

    private String theTitle;
    private String message;
    SharedPreferences sharedpreferences;
    byte[] bytes1;
    byte[] bytes2;
    byte[] bytes3;
    byte[] bytes4;
    byte[] bytes5;
    byte[] bytes6;
    byte[] bytes7;
    byte[] bytes8;
    byte[] bytes;

    public SOK_1_CLIENT(Context context,int nInstP,int nInstS,int nTime,int nTonality,int nTonalityBis, int nTempo, String title){
        this.context=context;

        theTitle = title;

        if(nTime > 127) {
            nbreTime1 = 127;
            nbreTime2 = nTime - 127;
        }
        else {
            nbreTime1 = nTime;
            nbreTime2 = 0;
        }

        if(nTempo > 127) {
            nbreTempo1 = 127;
            nbreTempo2 = nTempo - 127;
        }
        else {
            nbreTempo1 = nTempo;
            nbreTempo2 = 0;
        }

        bytes1 = ByteBuffer.allocate(4).putInt(nInstP).array();
        bytes2 = ByteBuffer.allocate(4).putInt(nInstS).array();
        bytes3 = ByteBuffer.allocate(8).putInt(nbreTime1).array();
        bytes4 = ByteBuffer.allocate(8).putInt(nbreTime2).array();
        bytes5 = ByteBuffer.allocate(4).putInt(nTonality).array();
        bytes6 = ByteBuffer.allocate(4).putInt(nTonalityBis).array();
        bytes7 = ByteBuffer.allocate(8).putInt(nbreTempo1).array();
        bytes8 = ByteBuffer.allocate(8).putInt(nbreTempo2).array();
     //   bytes7 = ByteBuffer.allocate(4).putInt(0).array();

        bytes = new byte[48];
        System.arraycopy(bytes1, 0, bytes, 0, 4);
        System.arraycopy(bytes2, 0, bytes, 4, 4);
        System.arraycopy(bytes3, 0, bytes, 8, 8);
        System.arraycopy(bytes4, 0, bytes, 16, 8);
        System.arraycopy(bytes5, 0, bytes, 24, 4);
        System.arraycopy(bytes6, 0, bytes, 28, 4);
        System.arraycopy(bytes7, 0, bytes, 32, 8);
        System.arraycopy(bytes8, 0, bytes, 40, 8);

        System.out.println(bytes);

        byte [] subArray = Arrays.copyOfRange(bytes, 20,48);

        System.out.println(Arrays.toString(subArray));

        ByteBuffer wrapped = ByteBuffer.wrap(subArray); // big-endian by default
        int num = wrapped.getInt(); // 1

        System.out.println(num);

    }

    public void run() {

        try {
            String adrress="88.177.158.216"; //localhost -- samemachine
            Socket SOCK =new Socket(adrress,5000);

            SOCK.getOutputStream().write(bytes, 0, 48);
            SOCK.getOutputStream().flush();

            System.out.println("Message sent to the server");

            InputStream in = SOCK.getInputStream();

            //internal storag

         /*   sharedpreferences = context.getSharedPreferences("nbreMusic", Context.MODE_PRIVATE);
            compt = sharedpreferences.getInt("nbre", 0);

            if (compt == 0) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt("nbre",2);
                editor.commit();
                compt = 1;
            } else {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt("nbre",compt+1);
                editor.commit();
            } */

            // nameFichier = "New music " + String.valueOf(compt) + ".mid";
            nameFichier = theTitle + ".mid";
           // nameFichier = "GeneratedMusic.mid";

         //   fileM =  context.openFileOutput(nameFichier,context.MODE_PRIVATE);

            //external storage
            String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.mediaMusics";
          //  String fileName = "newMusic.mid";

            File f = new File(baseDir + File.separator + nameFichier);

            FileOutputStream fileM = new FileOutputStream(f);

            byte buf[] = new byte[1024];

            int n;
            while((n=in.read(buf))!=-1)
                fileM.write(buf,0,n);

           // System.out.println(test);

            SOCK.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
