package com.example.martin.musicr;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.media.Ringtone;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.util.DisplayMetrics;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {  // implements NavigationView.OnNavigationItemSelectedListener

    private ImageButton Im1;
    private ImageButton Im2;
    private ImageButton Im3;
    private ImageButton Im4;

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private SOK_1_CLIENT test;
    Thread SOK_1_CLIENT = null;

    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Font path
       // String fontPath = "fonts/Face Your Fears.ttf";

        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(),"Debby.ttf");

        // Applying font
       // txtGhost.setTypeface(tf);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setElevation(0);
        }

        SharedPreferences preferences = getSharedPreferences("firstinit", Context.MODE_PRIVATE);
        if(!preferences.contains("ringtone")) {

            Intent alarmIntent = new Intent(MainActivity.this, AlarmReceiver.class);
            pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, alarmIntent, 0);

            AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            int interval = 24*60*60*1000;

            manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);

            System.out.println("*****************************************init*******************************");
        }

     /*   int actionBarTitle = getResources().getIdentifier("action_bar_title", "id",getPackageName());
        TextView actionBarTitleView = (TextView) getWindow().findViewById(actionBarTitle);
        //Typeface forte = Typeface.createFromAsset(getAssets(), "fonts/FORTE.TTF");
        if(actionBarTitleView != null){
            actionBarTitleView.setTypeface(tf);
            System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            actionBarTitleView.setTextSize(25);
        }*/

        SpannableString s = new SpannableString("Shuffle Music");
        s.setSpan(Typeface.createFromAsset(getAssets(),"Debby.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Update the action bar title with the TypefaceSpan instance
      /*  ActionBar actionBar = getActionBar();
        actionBar.setTitle(s);*/

     //   getActionBar().setTitle("SAMPLE");
        getSupportActionBar().setTitle(s);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

      /*  FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }); */

    /*    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        Im1 = (ImageButton) navigationView.findViewById(R.id.imageButton);
        Im2 = (ImageButton) navigationView.findViewById(R.id.imageButton2);
        Im3 = (ImageButton) navigationView.findViewById(R.id.imageButton3);
        Im4 = (ImageButton) navigationView.findViewById(R.id.imageButton4);

        Im1.setSelected(true);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        clickManager();

		// Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            contentMain firstFragment = new contentMain();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            firstFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, firstFragment).commit();
        }
        */

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new contentMain(), "MY MUSIC");
        adapter.addFragment(new ParamRingtone(), "GENERATE");
        adapter.addFragment(new SettingsMusics(), "SETTINGS");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

  /*  public void clickManager() {

        Im1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contentMain newFragment = new contentMain();

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.fragment_container, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                if (!Im1.isSelected()) {
                    Im1.setSelected(true);
                    Im2.setSelected(false);
                    Im3.setSelected(false);
                    Im4.setSelected(false);
                }

            }
        });

        Im2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ParamRingtone newFragment = new ParamRingtone();

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.fragment_container, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                if (!Im2.isSelected()) {
                    Im2.setSelected(true);
                    Im1.setSelected(false);
                    Im3.setSelected(false);
                    Im4.setSelected(false);
                }
            }
        });
        Im3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SOK_1_CLIENT = new Thread(new SOK_1_CLIENT(getApplicationContext()));
               // SOK_1_CLIENT.setContext(getApplicationContext());
                SOK_1_CLIENT.start();

                if (!Im3.isSelected()) {
                    Im3.setSelected(true);
                    Im2.setSelected(false);
                    Im1.setSelected(false);
                    Im4.setSelected(false);
                }
            }

        });
    } */

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

 /*   @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            //Intent intent = new Intent(MainActivity.this, paramRingtoneActivity.class);
            //startActivity(intent);
			
			// Create fragment and give it an argument specifying the article it should show
			ParamRingtone newFragment = new ParamRingtone();
			//Bundle args = new Bundle();
			//args.putInt(paramRingtone.ARG_POSITION, position);
			//newFragment.setArguments(args);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.fragment_container, newFragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
			
        } else if (id == R.id.nav_gallery) {
            //Intent intent = new Intent(MainActivity.this, paramRingtoneActivity.class);
            //startActivity(intent);

            // Create fragment and give it an argument specifying the article it should show
            contentMain newFragment = new contentMain();
            //Bundle args = new Bundle();
            //args.putInt(paramRingtone.ARG_POSITION, position);
            //newFragment.setArguments(args);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.fragment_container, newFragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*/
}
