package com.example.martin.musicr;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by martin on 14/01/2016.
 */
public class MusicAdapter extends ArrayAdapter<Music> {

    private List<Music> musicList;
    private Context context;

    public MusicAdapter(List<Music> musicList, Context ctx) {
        super(ctx, R.layout.row_layout, musicList);
        this.musicList = musicList;
        this.context = ctx;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        // First let's verify the convertView is not null
        if (convertView == null) {
            // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_layout, parent, false);
        }
        // Now we can fill the layout with the right values
        TextView tv = (TextView) convertView.findViewById(R.id.musicName);
       // TextView distView = (TextView) convertView.findViewById(R.id.dist);
        Music p = musicList.get(position);

        tv.setText(p.getName());
      //  distView.setText("" + p.getDistance());


        return convertView;
    }
}